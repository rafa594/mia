#include <stdlib.h>
#include "automata.h"
#include "estructuras.h"
#include "funciones.h"
/*Atributos:
    1 = size
    2 = path
    3 = name
    4 = id
    5 = idnum
    6 = unit
    7 = type
    8 = fit
    9 = delete
    10 = add
    11 = fs

*/
/* comandos:
    1 = mkdisk
    2 = rmdisk
    3 = fdisk
    4 = mount
    5 = umount
    6 = exec
    7 = rep
    8 = mkfs

*/
void limpiarBufer(char *cadena, int tamanio){
    int i;
    for (i=0;i<tamanio;i++)
        cadena[i]='\0';
    return;
}

void limpiar (char *cadena)
{
    char* s;
    s = strrchr(cadena,' ');
    if(s){
        if(s[1]=='\r'||s[1]=='\0'||s[1]=='\n'){
            *s = '\0';
            limpiar(cadena);
        }
    }

  char *p;
  p = strchr (cadena, '\n');
  if (p){
    *p = '\0';
    char *pp = strchr (cadena, '\r');
    if(pp)
        *pp = '\0';
    char *ppp = strchr (cadena, '\r');
    if(ppp)
        *ppp = '\0';
  }
}
void leer(char *cadena){
    limpiar(cadena);
    if (strcasecmp(cadena,"mount")==0){
        listarMount();
        return;
    }
    int tipoFDISK = 0;
    int tipoMKFS = 0;
    atributeStruct atributos;
    atributos.path = (char*)malloc(100);
    atributos.name = (char*)malloc(100);
    atributos.type = (char*)malloc(5);
    atributos.fit = (char*)malloc(3);
    atributos.id = (char*)malloc(5);
    atributos.fs = (char*)malloc(4);
    atributos.deleteAtr= (char*)malloc(5);
    int id = 1;
    int obligatorio=-1;
    int atributo = 0;
    int estado=0;
    int i=0;
    int j=0;
    int operacion=0;
    char bufer[50];
    limpiarBufer(bufer,50);

    while(cadena[i]!='\0'){

        switch(estado){

            case 0:

                if(cadena[i]==' '){
                estado=0;
                i++;
                }else if(cadena[i]=='#'){
                printf("Comentario.\n");
                estado=100;
                i++;
                }
                else if((cadena[i]>64 && cadena[i]<91)||(cadena[i]>96 &&cadena[i]<123)){
                    estado=1;
                    bufer[j]=cadena[i];
                    i++;
                    j++;
                }
                else{
                    printf("Comando incorrecto.\n");
                    return;
                }
                break;
            case 1:
                if((cadena[i]>64 && cadena[i]<91)||(cadena[i]>96 &&cadena[i]<123)){
                    estado=1;
                    bufer[j]=cadena[i];
                    i++;
                    j++;
                }else if(cadena[i]==' '){
                estado = 50;
                }
                else{
                    printf("Comando incorrecto.\n");
                    return;
                }
                break;
            case 2:
                if(cadena[i]=='-'){
                    //obligatorio
                    obligatorio = 1;
                    estado = 3;
                    i++;
                }else if(cadena[i]=='+'){
                    //opcional
                    obligatorio = 0;
                    estado = 4;
                    i++;
                }else if(cadena[i]==' '){
                    estado = 2;
                    i++;
                }else if(cadena[i]=='\0'){
                    estado = 0;
                    i++;
                }else if(cadena[i]=='\\'&& cadena[i+1]==13 && cadena[i+2]==10){
                    estado = 2;
                    i=i+3;
                }
                else {
                    printf("Comando incorrecto.\n");
                    return;
                }

                break;

            case 3:
                if((cadena[i]>64 && cadena[i]<91)||(cadena[i]>96 &&cadena[i]<123)){
                    estado=3;
                    bufer[j]=cadena[i];
                    i++;
                    j++;
                }else if(cadena[i]==':'){
                    estado = 5;
                    i++;
                }else if((cadena[i]>48) &&(cadena[i]<58)){
                    bufer[j]='\0';
                    if(strcasecmp(bufer,"id")==0){
                        bufer[j] == cadena[i];
                        j++;
                        operacion = 5;
                        estado = 7;
                        i++;
                        break;
                    }
                }
                else{
                    printf("Comando incorrecto.\n");
                    return;
                }
                break;
            case 4:
                if((cadena[i]>64 && cadena[i]<91)||(cadena[i]>96 &&cadena[i]<123)){
                    estado = 4;
                    bufer[j]=cadena[i];
                    i++;
                    j++;
                }else if(cadena[i]==':'){
                    estado = 6;
                    i++;
                }else{
                    printf("Comando incorrecto.\n");
                    return;
                }
                break;

            case 5:
                if(cadena[i]==':'){
                    i++;
                    bufer[j]='\0';
                    j=0;
                    if(strcasecmp(bufer,"size")==0){
                        atributo = 1;
                        switch(operacion){
                            case 1:
                            case 3:
                                estado = 11;//numero
                                break;
                            default:
                                printf("Comando incorrecto.\n");
                                return;
                        }

                        break;
                    }else if(strcasecmp(bufer,"path")==0){
                        atributo = 2;

                        switch(operacion){
                            case 1:
                            case 2:
                            case 3:
                            case 4:
                            case 6:
                            case 7:
                                estado = 10;//path
                                break;
                            default:
                                printf("Comando incorrecto.\n");
                                return;
                        }


                        break;
                    }else if(strcasecmp(bufer,"name")==0){
                        atributo = 3;
                        limpiarBufer(bufer,50);
                        j=0;
                        switch(operacion){
                            case 1:
                            case 3:
                            case 4:
                            case 7:
                                estado = 10;//path
                                break;
                            default:
                                printf("Comando incorrecto.\n");
                                return;
                        }
                        break;
                    }else if(strcasecmp(bufer,"id")==0){
                        atributo = 3;
                        limpiarBufer(bufer,50);
                        j=0;
                        switch(operacion){
                            case 3:
                            case 4:
                            case 7:
                            case 8:
                                i--;
                                estado = 17;//
                                break;
                            default:
                                printf("Comando incorrecto.\n");
                                return;
                        }
                        break;
                    }
                    else{
                        printf("Comando incorrecto.\n");
                        return;
                    }

                }else{
                    printf("Comando incorrecto.\n");
                    return;
                }
                break;

            case 6:
                if(cadena[i]==':'){
                    i++;
                    bufer[j]='\0';
                    j=0;
                    if(strcasecmp(bufer,"unit")==0){

                        atributo = 6;
                        switch(operacion){
                            case 1:
                            case 3:
                            case 8:
                                estado = 12;//char
                                break;
                            default:
                                printf("Comando incorrecto.\n");
                                return;
                        }

                        break;
                    }else if(strcasecmp(bufer,"delete")==0){
                        atributo = 9;
                        if (tipoFDISK!=0)
                            tipoFDISK=-1;
                            else tipoFDISK =1;

                        switch(operacion){
                            case 1:
                            case 3:
                                estado = 15;//char
                                break;
                            default:
                                printf("Comando incorrecto.\n");
                                return;
                        }

                        break;
                    }else if(strcasecmp(bufer,"add")==0){
                        atributo = 10;
                        if (tipoFDISK!=0)
                            tipoFDISK=-1;
                            else tipoFDISK =2;
                        if (tipoMKFS!=0)
                            tipoMKFS=-1;
                            else tipoMKFS =1;

                        switch(operacion){
                            case 1:
                            case 3:
                                estado = 11;//char
                                break;
                            default:
                                printf("Comando incorrecto.\n");
                                return;
                        }

                        break;
                    }else if(strcasecmp(bufer,"type")==0){
                        atributo = 7;

                        switch(operacion){
                            case 3:
                            case 8:
                                estado = 15;//cadena sin comillas
                                break;
                            default:
                                printf("Comando incorrecto.\n");
                                return;
                        }

                        break;
                    }else if(strcasecmp(bufer,"fit")==0){
                        atributo = 8;

                        switch(operacion){
                            case 3:
                                estado = 15;//cadena sin comillas
                                break;
                            default:
                                printf("Comando incorrecto.\n");
                                return;
                        }

                        break;
                    }else if(strcasecmp(bufer,"FS")==0){
                        atributo = 11;

                        switch(operacion){
                            case 8:

                                estado = 15;//cadena sin comillas
                                break;
                            default:
                                printf("Comando incorrecto.\n");
                                return;
                        }

                        break;
                    }

                    else{
                    printf("Comando incorrecto.\n");
                    return;
                    }

                }else{
                    printf("Comando incorrecto.\n");
                    return;
                }
                break;

            case 7:
                if((cadena[i]>48) &&(cadena[i]<58)){
                    bufer[j]=cadena[i];
                    i++;
                    j++;
                    estado=7;
                }else if(cadena[i]==':'){
                    limpiarBufer(bufer,50);
                    j=0;
                    estado = 17;
                    i++;
                }else{
                    printf("Comando incorrecto.\n");
                    return;
                }
                break;

            case 10:
                if(cadena[i]=='"'){
                    limpiarBufer(bufer,50);
                    j=0;
                    estado = 20;
                    i++;
                }else{
                    printf("Comando incorrecto.\n");
                    return;
                }
                break;

           case 15:
                if((cadena[i]>64 && cadena[i]<91)||(cadena[i]>96 &&cadena[i]<123)){
                    estado = 15;
                    bufer[j]=cadena[i];
                    j++;
                    if(cadena[i+1]=='\0')
                        estado = 16;
                    else i++;

                }else if(cadena[i]==' '){
                    estado = 16;
                }else{
                    printf("Comando incorrecto.\n");
                    return;
                }


                break;
            case 16:
                bufer[j]='\0';
                i++;
                if(atributo==7){
                    strcpy(atributos.type, bufer);
                    estado = 2;
                }else if(atributo==8){
                    strcpy(atributos.fit, bufer);
                    estado = 2;
                }else if(atributo==9){
                    strcpy(atributos.deleteAtr, bufer);
                    estado = 2;
                }else if(atributo==11){
                    strcpy(atributos.fs, bufer);
                    estado = 2;
                }
                else{
                    printf("Comando incorrecto.\n");
                    return;
                }
                limpiarBufer(bufer,50);
                    j=0;
                break;
            case 11:
                if((cadena[i]>47) &&(cadena[i]<58)||(cadena[i]=='-')){
                    bufer[j]=cadena[i];
                    j++;
                    estado=11;
                    if(cadena[i+1]=='\0'){
                        estado = 22;
                    }else i++;
                }else if(cadena[i]== ' '){
                    estado = 22;
                }else{
                    printf("Comando incorrecto.\n");
                    return;
                }
                break;
            case 12:
                if((cadena[i]>64 && cadena[i]<91)||(cadena[i]>96 &&cadena[i]<123)){
                    estado = 2;

                    j=0;
                    limpiarBufer(bufer,50);
                    if (atributo == 6){
                        atributos.unit = cadena[i];
                        i++;
                    }else{
                        printf("Comando incorrecto.\n");
                        return;
                    }
                }else{
                    printf("Comando incorrecto.\n");
                    return;
                }
                break;
            case 20:
                if (cadena[i]!='"'){
                    bufer[j]= cadena[i];
                    i++;
                    j++;
                    estado = 20;
                }else{
                    bufer[j]='\0';
                    estado = 21;

                }
                break;

            case 21:
            i++;
                if (atributo == 2){
                    strcpy(atributos.path,bufer);
                    estado =2;
                    j = 0;
                    limpiarBufer(bufer,50);
                }else if(atributo == 3){
                    strcpy(atributos.name,bufer);
                    estado =2;
                    j = 0;
                    limpiarBufer(bufer,50);
                }else{
                    printf("Comando incorrecto.\n");
                    return;
                }


                break;
            case 22:
                bufer[j]='\0';i++;
                switch(atributo){
                    case 1:
                        atributos.size = atoi(bufer);
                        j = 0;
                        limpiarBufer(bufer,50);
                        estado = 2;
                        break;
                    default:
                        printf("Comando incorrecto.\n");
                        return;
                }

                break;
            case 17:
                if (cadena[i]==':'){
                    i++;
                    if((cadena[i]=='v'||cadena[i]=='V')&&(cadena[i+1]=='d'||cadena[i+1]=='D')){
                        bufer[j]=cadena[i];
                        i++;j++;
                        bufer[j]=cadena[i];
                        i++;j++;
                        if((cadena[i]>64 && cadena[i]<91)||(cadena[i]>96 &&cadena[i]<123)){
                            bufer[j]=cadena[i];
                            i++;
                            j++;
                            while((cadena[i]>48) &&(cadena[i]<58)){
                                bufer[j] = cadena[i];
                                i++;
                                j++;
                            }
                            estado = 18;
                            if(cadena[i]=='\0')
                                i--;
                            break;
                        }else{
                        printf("Comando incorrecto.\n");
                        return;
                        }

                    }else{
                        printf("Comando incorrecto.\n");
                        return;
                    }

                }else{
                    printf("Comando incorrecto.\n");
                    return;
                }

                break;
            case 18:
                bufer[j]='\0';
                j=0;
                strcpy(atributos.id,bufer);
                limpiarBufer(bufer,50);
                estado = 2;
                i++;
                break;

            case 50:
                bufer[j]='\0';
                if(strcasecmp(bufer,"mkdisk")==0){
                        atributos.unit = 'M';
                        operacion = 1;
                        estado = 2;
                        i++;
                        j=0;
                }else if(strcasecmp(bufer,"rmdisk")==0){
                        operacion = 2;
                        estado = 2;
                        i++;
                        j=0;
                }else if(strcasecmp(bufer,"fdisk")==0){
                        atributos.unit = 'K';
                        strcpy(atributos.type,"P");
                        strcpy(atributos.fit,"WF");
                        operacion = 3;
                        estado = 2;
                        i++;
                        j=0;

                }else if(strcasecmp(bufer,"mount")==0){
                        operacion = 4;
                        estado = 2;
                        i++;
                        j=0;

                }else if(strcasecmp(bufer,"umount")==0){
                        operacion = 5;
                        estado = 2;
                        i++;
                        j=0;

                }else if(strcasecmp(bufer,"exec")==0){
                        operacion = 6;
                        estado = 2;
                        i++;
                        j=0;

                }else if(strcasecmp(bufer,"rep")==0){
                        operacion = 7;
                        estado = 2;
                        i++;
                        j=0;

                }else if(strcasecmp(bufer,"mkfs")==0){
                        operacion = 8;
                        strcpy(atributos.fs,"3fs");
                        atributos.unit = 'K';
                        strcpy(atributos.type,"full");

                        estado = 2;
                        i++;
                        j=0;

                }
                else{
                    printf("amm");
                    return;
                }
                break;
            case 100:
                if(cadena[i]!='\0'){
                    i++;
                    estado = 100;
                    break;
                }
            default:

                break;
        }//switch


    }//while
    switch (operacion){
        case 1:
            printf("mkdisk\n");
            mkdisk(atributos);
            return;
        case 2:
            printf("rmdisk\n");
            rmdisk(atributos);
            return;
        case 3:
            printf("fdisk\n");
            fdisk(atributos,tipoFDISK);
            return;
        case 4:
            printf("mount\n");
            mount(atributos);
            return;
        case 5:
            printf("umount\n");
            umount(atributos);
            return;
        case 6:
            printf("exec\n");
            exec(atributos);
            return;
        case 7:
            printf("rep\n");
            rep(atributos);
            return;
        case 8:
            printf("mkfs\n");
            mkfs(atributos,tipoMKFS);
            return;
        break;
    }


}
