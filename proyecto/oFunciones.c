#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include "funciones.h"
#include "estructuras.h"
#include "automata.h"



void mkfs(atributeStruct n,int tipo){
    if (tipo==1){
        printf("mkfs add \n");
    }else if(tipo == 0){

        if (strcasecmp(n.fs,"2fs")==0){
            mkfsEXT2(n);
            printf("Ext2 \n");
            }
        else if (strcasecmp(n.fs,"3fs")==0){
            mkfsEXT3(n);
            printf("Ext3 \n");
            }
    }



}
void crearCarpeta(char* path, atributeStruct n,partition part){
    if(strcasecmp(path,"/")==0){
        FILE* archOpen = fopen(path,"rb+");
        if(archOpen){




            fclose(archOpen);
        }else{
            printf("Problema al abrir el archivo.\n");
            return;
        }
    }
}
void mkfsEXT3(atributeStruct n){

if(n.id==NULL){
        printf("Error en atributo -id.\n");
    }
    nodoParticion* aa= existeParticionMontadaId(n.id);
    if (aa==NULL){
        printf("Particion %s no ha sido montada. \n", n.id);
        return;
    }
    nodoDisco* disk = getIdDiscoMontado(n.id[2]);
    if(disk!=NULL){
        FILE* archOpen = fopen(disk->path,"rb+");
        if(archOpen){
            mbr mbrRead;
            fread(&mbrRead,sizeof(mbr),1,archOpen);
            partition part;
            part = posicionNombreParticionPrimaria(mbrRead,aa->name);
            if(part.part_start>0){
                //aqui va la posicion de una particion primaria
                superBloque super;
                int nEstructuras = (part.part_size-sizeof(superBloque))/(4 +
                    sizeof(journal)+sizeof(inodo)+3*sizeof(bloqueArchivo));
                super.s_filesystem_type=3;
                super.s_inodes_count=nEstructuras;
                super.s_blocks_count=3*nEstructuras;
                super.s_free_inodes_count = nEstructuras;
                super.s_free_blocks_count=3*nEstructuras;
                super.s_mtime = super.s_umtime =  time(0);
                super.s_mnt_count = 1;
                super.s_magic = 61267;
                super.s_inode_size = sizeof(inodo);
                super.s_block_size = sizeof(bloqueApuntadores);
                super.s_first_ino = 0;
                super.s_first_blo = 0;
                super.s_bm_inode_start = part.part_start + sizeof(superBloque)+nEstructuras*sizeof(journal);
                super.s_bm_block_start = super.s_bm_inode_start + super.s_inodes_count;
                super.s_inode_start = super.s_block_start+super.s_blocks_count;
                super.s_block_start = super.s_inode_start + super.s_inodes_count*sizeof(inodo);
                char bmtmp[nEstructuras];
                journal jn;
                fseek(archOpen,part.part_start,SEEK_SET);
                fwrite(&super,sizeof(superBloque),1,archOpen);
                fwrite(&jn,sizeof(journal),nEstructuras,archOpen);
                inodo in;
                bloqueArchivo bl;
                int xx;
                for (xx=0;xx<nEstructuras;xx++)
                    bmtmp[xx]='0';
                fwrite(bmtmp,nEstructuras,4,archOpen);
                fwrite(&in,sizeof(inodo),super.s_inodes_count,archOpen);
                fwrite(&bl,sizeof(bloqueArchivo),super.s_blocks_count,archOpen);

                printf("Particion formateada exitosamente.\n");
                fclose(archOpen);
                return;

            }else if(part.part_start==-2){
                printf("No se puede formatear una particion extendida.\n");
                fclose(archOpen);
                return;
            }else{

               //logica


            }

            fclose(archOpen);
        }else{
            printf("Problema al abrir el archivo.\n");
            return;
        }
    }else{
        printf("Disco no montado.\n");
        return;
    }
}
void mkfsEXT2(atributeStruct n){

if(n.id==NULL){
        printf("Error en atributo -id.\n");
    }
    nodoParticion* aa= existeParticionMontadaId(n.id);
    if (aa==NULL){
        printf("Particion %s no ha sido montada. \n", n.id);
        return;
    }
    nodoDisco* disk = getIdDiscoMontado(n.id[2]);
    if(disk!=NULL){
        FILE* archOpen = fopen(disk->path,"rb+");
        if(archOpen){
            mbr mbrRead;
            fread(&mbrRead,sizeof(mbr),1,archOpen);
            partition part;
            part = posicionNombreParticionPrimaria(mbrRead,aa->name);
            if(part.part_start>0){
                //aqui va la posicion de una particion primaria
                superBloque super;
                int nEstructuras = (part.part_size-sizeof(superBloque))/(4 + sizeof(inodo)+3*sizeof(bloqueArchivo));
                super.s_filesystem_type=2;
                super.s_inodes_count=nEstructuras;
                super.s_blocks_count=3*nEstructuras;
                super.s_free_inodes_count = nEstructuras;
                super.s_free_blocks_count=3*nEstructuras;
                super.s_mtime = super.s_umtime =  time(0);
                super.s_mnt_count = 1;
                super.s_magic = 61267;
                super.s_inode_size = sizeof(inodo);
                super.s_block_size = sizeof(bloqueApuntadores);
                super.s_first_ino = 0;
                super.s_first_blo = 0;
                super.s_bm_inode_start = part.part_start + sizeof(superBloque);
                super.s_bm_block_start = super.s_bm_inode_start + super.s_inodes_count;
                super.s_inode_start = super.s_block_start+super.s_blocks_count;
                super.s_block_start = super.s_inode_start + super.s_inodes_count*sizeof(inodo);
                char bmtmp[nEstructuras];
                fseek(archOpen,part.part_start,SEEK_SET);
                fwrite(&super,sizeof(superBloque),1,archOpen);
                inodo in;
                bloqueArchivo bl;
                int xx;
                for (xx=0;xx<nEstructuras;xx++)
                    bmtmp[xx]='0';
                fwrite(bmtmp,nEstructuras,4,archOpen);
                fwrite(&in,sizeof(inodo),super.s_inodes_count,archOpen);
                fwrite(&bl,sizeof(bloqueArchivo),super.s_blocks_count,archOpen);



                printf("Particion formateada exitosamente.\n");
                fclose(archOpen);

                return;

            }else if(part.part_start==-2){
                printf("No se puede formatear una particion extendida.\n");
                fclose(archOpen);
                return;
            }else{

               //logica


            }

            fclose(archOpen);
        }else{
            printf("Problema al abrir el archivo.\n");
            return;
        }
    }else{
        printf("Disco no montado.\n");
    }
}


