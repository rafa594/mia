#ifndef ESTRUCTURAS_H_INCLUDED
#define ESTRUCTURAS_H_INCLUDED
#include <time.h>

    typedef struct sb{
        int s_filesystem_type;
        int s_inodes_count;
        int s_blocks_count;
        int s_free_blocks_count;
        int s_free_inodes_count;
        time_t s_mtime;
        time_t s_umtime;
        int s_mnt_count;
        int s_magic;
        int s_inode_size;
        int s_block_size;
        int s_first_ino;
        int s_first_blo;
        int s_bm_inode_start;
        int s_bm_block_start;
        int s_inode_start;
        int s_block_start;
        int s_journal_count;
    }superBloque;

    typedef struct jlng{
        char journal_tipo_operacion;
        char journal_tipo;
        char journal_nombre[12];
        char journal_contenido[64];
        time_t journal_fecha;
        int journal_propietario;
        int journal_permisos;
    }journal;

    typedef struct in{
        int i_nlink;
        char i_pathlink[50];
        int i_uid;
        int i_gid;
        int i_size;
        time_t i_atime;
        time_t i_ctime;
        time_t i_mtime;
        int i_block[15];
        char i_type;
        int i_perm;
    }inodo;

    typedef struct ctnt{
        char b_name[12];
        int b_inodo;
    }content;

    typedef struct arch{
        char b_content[64];
    }bloqueArchivo;

    typedef struct bc{
        content b_content[4];
    }bloqueCarpeta;

    typedef struct ba{
        int b_pointers[16];
    }bloqueApuntadores;



    typedef struct dsc{
        char id[10];
        char nombre[33];
        int tamanio;
        int primarias;
        int extendidas;
        int logicas;
        int sinParticionar;
        int estado;
    }disco;

        typedef struct part{
        char part_status;
        char part_type;
        char part_fit[2];
        int part_start;
        int part_size;
        char part_name[16];
    }partition;

    typedef struct estInicio{
        int mbr_tamanio;
        time_t mbr_fecha_creacion;
        int mbr_disk_signature;
        partition mbr_partition_1;
        partition mbr_partition_2;
        partition mbr_partition_3;
        partition mbr_partition_4;
    }mbr;
    typedef struct eb{
        char part_status;
        char part_fit[2];
        int part_start;
        int part_size;
        int part_next;
        char part_name[16];
    }ebr;

    typedef struct {
        int size;//tamaño
        char unit;//m,k
        char* path;//ruta
        char* type;//tipo = fast/full - P/E/L
        char* fit;//F/W/B
        char* deleteAtr;//fast/full
        char* name; //nombre particion
        int add; //unidades, negativas y positivas
        char* id;
        int p;
        char* cont;
        char* dest;
        char* iddest;
        int error;
        char * fs;
    } atributeStruct;

    typedef struct nP{
        char* name;
        char* idParticion;
        int id;
        partition part;
        ebr ebrPart;
        struct nP* siguiente;
    } nodoParticion;
    typedef struct nD{
        int particiones;
        char letra;
        char* path;
        char* idDisco;
        nodoParticion* inicio;
        struct nD* siguiente;
    } nodoDisco;
    typedef struct{
        nodoDisco* inicio;
        int discos;
    } discMont;
    discMont* montados;

#endif // ESTRUCTURAS_H_INCLUDED
