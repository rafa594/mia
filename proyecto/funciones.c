#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include "funciones.h"
#include "estructuras.h"
#include "automata.h"
char* fecha(time_t tiempo) {
    char* t = malloc(19);
    struct tm *tlocal = localtime(&tiempo);
    strftime(t, 19, "%d/%m/%y--%H:%M:%S", tlocal);
    //printf("%s",t);
    return t;
}
void eliminarDiscoMontado(nodoDisco* n){
    if(montados!=NULL){
        if(montados->inicio==n){
            montados->inicio = n->siguiente;
            montados->discos--;
            if((montados->inicio==NULL)||(montados->discos==0)){
                montados = NULL;
            }
        }else{
            nodoDisco* piv = montados->inicio;
            while(piv->siguiente!=NULL){
                if(piv->siguiente==n){
                    piv->siguiente=n->siguiente;
                    montados->discos--;

                }else{
                    piv = piv->siguiente;
                }
            }
        }
    }
}
int existeNombreParticionPrimariaExtendida(mbr m,partition p){
    if (m.mbr_partition_1.part_status=='1'){
        if(strcmp(m.mbr_partition_1.part_name,p.part_name)==0)
            return 1;
    }
    if (m.mbr_partition_2.part_status=='1'){
        if(strcmp(m.mbr_partition_2.part_name,p.part_name)==0)
            return 2;
    }
    if (m.mbr_partition_3.part_status=='1'){
        if(strcmp(m.mbr_partition_3.part_name,p.part_name)==0)
            return 3;
    }
    if (m.mbr_partition_4.part_status=='1'){
        if(strcmp(m.mbr_partition_4.part_name,p.part_name)==0)
            return 4;
    }

    return 0;
}
partition posicionNombreParticionPrimaria(mbr m,char* name){
    partition n;

    if (m.mbr_partition_1.part_status=='1'){
        if(strcmp(m.mbr_partition_1.part_name,name)==0)
            if(m.mbr_partition_1.part_type=='P'||m.mbr_partition_1.part_type=='p')
            return m.mbr_partition_1;
            else{
                n.part_start=-2;
                return n;
            }
    }
    if (m.mbr_partition_2.part_status=='1'){
        if(strcmp(m.mbr_partition_2.part_name,name)==0)
            if(m.mbr_partition_2.part_type=='P'||m.mbr_partition_2.part_type=='p')
            return m.mbr_partition_2;
            else{
                n.part_start=-2;
                return n;
            }


    }
    if (m.mbr_partition_3.part_status=='1'){
        if(strcmp(m.mbr_partition_3.part_name,name)==0)
            if(m.mbr_partition_3.part_type=='P'||m.mbr_partition_3.part_type=='p')
            return m.mbr_partition_3;
            else{
                n.part_start=-2;
                return n;
            }
    }
    if (m.mbr_partition_4.part_status=='1'){
        if(strcmp(m.mbr_partition_4.part_name,name)==0)
            if(m.mbr_partition_4.part_type=='P'||m.mbr_partition_4.part_type=='p')
            return m.mbr_partition_4;
            else{
                n.part_start=-2;
                return n;
            }
    }
    n.part_start=0;
    return n;
}
partition posicionParticionExtendida(mbr m){
    partition n;
    n.part_start=0;
    if (m.mbr_partition_1.part_status=='1'){
            if(m.mbr_partition_1.part_type=='E'||m.mbr_partition_1.part_type=='e')
            return m.mbr_partition_1;

    }
    if (m.mbr_partition_2.part_status=='1' ){
            if(m.mbr_partition_2.part_type=='E'||m.mbr_partition_2.part_type=='e')
            return m.mbr_partition_2;
    }
    if (m.mbr_partition_3.part_status=='1'){

            if(m.mbr_partition_3.part_type=='E'||m.mbr_partition_3.part_type=='e')
            return m.mbr_partition_3;
    }
    if (m.mbr_partition_4.part_status=='1'){

            if(m.mbr_partition_4.part_type=='E'||m.mbr_partition_4.part_type=='e')
            return m.mbr_partition_4;
    }

    return n;
}

nodoParticion* getParticionMontada(nodoDisco* discoE,char* id){
    if (discoE->particiones==0)
        return NULL;
    else{
        nodoParticion* aux = discoE->inicio;
        while(aux!=NULL){
            if((strcasecmp(aux->idParticion,id))==0)
                return aux;
            else
                aux=aux->siguiente;
        }
        return NULL;
    }
}
nodoParticion* getParticionMontadaName(nodoDisco* discoE,char* name){
    if (discoE->particiones==0)
        return NULL;
    else{
        nodoParticion* aux = discoE->inicio;
        while(aux!=NULL){
            if((strcasecmp(aux->name,name))==0)
                return aux;
            else
                aux=aux->siguiente;
        }
        return NULL;
    }
}
int existeParticionMontada(nodoDisco* discoE,int id){
    if (discoE->particiones==0)
        return 0;
    else{
        nodoParticion* aux = discoE->inicio;
        while(aux!=NULL){
            if(aux->id==id)
                return 1;
            else
                aux=aux->siguiente;
        }
        return 0;
    }
}
char* separarLetraIdDisco(char* id){
    const char i[2] = "vd";
    char* o;
    o=strtok(id,i);
    return o;
}
nodoDisco* getIdDiscoMontado(char letra){
    if(montados==NULL)
        return NULL;
    else{
        nodoDisco* aux = montados->inicio;
        while(aux!=NULL){
            if(aux->letra==letra)
                return aux;
            else
                aux=aux->siguiente;
        }
        return NULL;
    }
}

nodoParticion* existeParticionMontadaId(char* id){
    //char* o = separarLetraIdDisco(id);
    nodoDisco* discoE;
            discoE= getIdDiscoMontado(id[2]);
    if(discoE!=NULL){
        if (discoE->particiones==0)
        return NULL;
    else{
        nodoParticion* aux = discoE->inicio;
        while(aux!=NULL){
            if(aux->id==((int)id[3]-48))
                return aux;
            else
                aux=aux->siguiente;
        }
        return NULL;
    }
    }
    else    return NULL;
}

nodoDisco* getDiscoMontado(char* path){

    if(montados->discos==0)
        return NULL;
    else{
        nodoDisco* aux = montados->inicio;
        while(aux!=NULL){
            if(strcasecmp(aux->path,path)==0)
                return aux;
            else
                aux=aux->siguiente;
        }
        return NULL;
    }
}
int existeIdDiscoMontado(char letra){
    if(montados->discos==0)
        return 0;
    else{
        nodoDisco* aux = montados->inicio;
        while(aux!=NULL){
            if(aux->letra==letra)
                return 1;
            else
                aux=aux->siguiente;
        }
        return 0;
    }
}
nodoDisco* ultimoDiscoMontado(){
    if(montados->discos==0)
        return NULL;
    else{
        nodoDisco* aux = montados->inicio;
        while(aux->siguiente!=NULL){
                aux=aux->siguiente;
        }
        return aux;
    }
}
nodoParticion* ultimaParticionMontada(nodoDisco* n){
    if(n->particiones==0)
        return NULL;
    else{
        nodoParticion* aux = n->inicio;
        while(aux->siguiente!=NULL){
                aux=aux->siguiente;
        }
        return aux;
    }
}
int existeDiscoMontado(char* path){
    if(montados->discos==0)
        return 0;
    else{
        nodoDisco* aux = montados->inicio;
        while(aux!=NULL){
            if((strcasecmp(aux->path,path))==0)
                return 1;
            else
                aux=aux->siguiente;
        }
        return 0;
    }
}

char* buscarPadres(char* path){
    char* npath = (char*)malloc(100);
    char* xpath = (char*)malloc(100);
    strcpy(xpath,path);
    const char d[2] = "/";
    const char f[2] = ".";

    char* aux = strtok(xpath,d);
    while(aux!=NULL){

        if(strcmp(aux,path)==0)
            return NULL;
        if(strchr(aux,f[0])!=NULL)
            return npath;
        else{
            strcat(npath,d);
            strcat(npath,aux);
            aux = strtok(NULL,d);
        }
    }

}
void crearPadres(char* path){
    char* padres = buscarPadres(path);

    if(padres!=NULL){
        char* o = (char*)malloc(100);
        char* p = (char*)malloc(100);
        strcpy(o,"sudo mkdir ");
        strcpy(p,"\"");
        strcat(p,padres);
        strcat(p,"\"");
        strcat(o,p);
        strcat(o," -p");

        int x = system(o);


        if (x == -1) {
            printf("Error en la creacion de los padres.\n");
            return;
        }else{
            printf("Creados los padres.\n");
        }
        strcpy(o,"sudo chmod -R 777 ");
        strcat(o,p);
        x = system(o);

    }
    return;
}

void mkdisk(atributeStruct n){
    char cad[1024];
    int j;

    for(j=0; j<10; j++){cad[j]='\0';}

    mbr nuevo;
    nuevo.mbr_fecha_creacion=time(0);
    int mul;
    if(n.size>0){

        if (n.unit=='k')
            mul =1024;
        else if (n.unit=='K')
            mul =1024;
        else if(n.unit=='m')
            mul = 1024*1024;
        else if(n.unit=='M')
            mul = 1024*1024;
        else
        {printf("Error en atributo +unit: %c\n",n.unit); return;}
        nuevo.mbr_tamanio = n.size * mul;
        if(nuevo.mbr_tamanio<(10*1024*1024)){
            printf("Tamanio menor a 10 MB.\n");
            return;
        }
        nuevo.mbr_disk_signature=rand();
        nuevo.mbr_partition_1.part_status='0';
        nuevo.mbr_partition_2.part_status='0';
        nuevo.mbr_partition_3.part_status='0';
        nuevo.mbr_partition_4.part_status='0';
        char *rutaPadre= (char*)malloc(100);
        strcpy(rutaPadre,n.path);
        strcat(rutaPadre,n.name);
        crearPadres(rutaPadre);

        FILE *archivo=fopen(rutaPadre,"wb+");
        if(archivo)
        {
            fwrite(&nuevo, sizeof(mbr),1,archivo);

            int totalLlenar = nuevo.mbr_tamanio - sizeof(mbr);

            totalLlenar = totalLlenar/1024;

            int i;
            for(i=0;i<=totalLlenar;i++){
                fwrite(cad, sizeof(char)*1024,1,archivo);
            }
            fclose(archivo);
            printf("Disco %s creado correctamente. \n",rutaPadre);
        }else{
            printf("Problema al abrir el archivo %s. \n",rutaPadre);
        }
    }else{
        printf("El atributo -size tiene un valor menor o igual a 0: %d \n", n.size);
    }
}

void rmdisk(atributeStruct n){
     FILE *fichero;

   fichero = fopen(n.path, "rb" );    /* El fichero ha de existir primeramente */
   if( fichero )
   {
       fclose(fichero);
      if( remove(n.path) == 0 )   printf( "Borrado disco %s\n",n.path );
      else   printf( "Disco %s no pudo ser borrado\n",n.path );
   }
   else   printf( "El disco %s no existe\n", n.path);
   return;
}


void fdisk(atributeStruct n,int tipoFDISK){
    //0 crea,1 elimina, 2 agrega/quita
    int mul;
    if(tipoFDISK==-1){
        printf("Error en atributos incompatibles entre si. \n");
        tipoFDISK=0;
        return;
    }
    if(tipoFDISK ==0){
        if(n.size>0){

            if (n.unit=='k')
                mul =1024;
            else if (n.unit=='K')
                mul =1024;
            else if(n.unit=='m')
                mul = 1024*1024;
            else if(n.unit=='M')
                mul = 1024*1024;
            else if(n.unit=='b')
                mul = 1;
            else if(n.unit=='B')
                mul = 1;
            else
            {printf("Error en atributo -unit: %c\n",n.unit); return;}

            if (!((strcasecmp(n.type,"P")==0)||(strcasecmp(n.type,"E")==0)||
                    (strcasecmp(n.type,"L")==0)))
            {printf("Error en atributo -type: %s\n",n.type); return;}

            if (!((strcasecmp(n.fit,"FF")==0)||(strcasecmp(n.fit,"WF")==0)||
            (strcasecmp(n.fit,"BF")==0)))
            {printf("Error en atributo -fit: %s\n",n.fit); return;}


            FILE *archivo=fopen(n.path,"rb+");


            if(archivo)
            {
                mbr mbrRead;
                partition nPartition;
                fseek(archivo,0,SEEK_SET);
                fread(&mbrRead,sizeof(mbr),1,archivo);
                strcpy(nPartition.part_name,n.name);
                strcpy(nPartition.part_fit,n.fit);
                nPartition.part_size = n.size*mul;
                nPartition.part_type = n.type[0];
                nPartition.part_status = '1';
                if(existeNombreParticionPrimariaExtendida(mbrRead,nPartition)>0){
                    printf("Error: El nombre de la particion %s ya existe en este disco.\n",n.name);
                    return;
                }
                if((mbrRead.mbr_partition_1.part_status=='1')&&
                        ((mbrRead.mbr_partition_1.part_type=='e')||
                        (mbrRead.mbr_partition_1.part_type=='E'))){
                    ebr ebrR;
                    fseek(archivo,mbrRead.mbr_partition_1.part_start,SEEK_SET);
                    fread(&ebrR,sizeof(ebr),1,archivo);
                    while(ebrR.part_status!='0'){
                        if(strcmp(ebrR.part_name,n.name)==0){
                            printf("Error: El nombre de la particion %s ya existe en este disco.\n",n.name);
                            fclose(archivo);
                            return;
                        }
                        if(ebrR.part_next>0){
                            fseek(archivo,ebrR.part_next,SEEK_SET);
                            fread(&ebrR,sizeof(ebr),1,archivo);
                        }else break;
                    }
                    if (strcasecmp(n.type,"e")==0){
                        printf("Error: Ya existe una partición extendida en este disco.\n");
                        return;
                    }
                }
                else if((mbrRead.mbr_partition_2.part_status=='1')&&
                        ((mbrRead.mbr_partition_2.part_type=='e')||
                        (mbrRead.mbr_partition_2.part_type=='E'))){
                    ebr ebrR;
                    fseek(archivo,mbrRead.mbr_partition_2.part_start,SEEK_SET);
                    fread(&ebrR,sizeof(ebr),1,archivo);
                    while(ebrR.part_status!='0'){
                        if(strcmp(ebrR.part_name,n.name)==0){
                            printf("El nombre de la particion %s ya existe en este disco.\n",n.name);
                            fclose(archivo);
                            return;
                        }
                        if(ebrR.part_next>0){
                            fseek(archivo,ebrR.part_next,SEEK_SET);
                            fread(&ebrR,sizeof(ebr),1,archivo);
                        }else break;
                        if (strcasecmp(n.type,"e")==0){
                        printf("Error: Ya existe una partición extendida en este disco.\n");
                        return;
                        }
                    }
                }
                else if((mbrRead.mbr_partition_3.part_status=='1')&&
                        ((mbrRead.mbr_partition_3.part_type=='e')||
                        (mbrRead.mbr_partition_3.part_type=='E'))){
                    ebr ebrR;
                    fseek(archivo,mbrRead.mbr_partition_3.part_start,SEEK_SET);
                    fread(&ebrR,sizeof(ebr),1,archivo);
                    while(ebrR.part_status!='0'){
                        if(strcmp(ebrR.part_name,n.name)==0){
                            printf("Error: El nombre de la particion %s ya existe en este disco.\n",n.name);
                            fclose(archivo);
                            return;
                        }
                        if(ebrR.part_next>0){
                            fseek(archivo,ebrR.part_next,SEEK_SET);
                            fread(&ebrR,sizeof(ebr),1,archivo);
                        }else break;
                        if (strcasecmp(n.type,"e")==0){
                        printf("Error: Ya existe una partición extendida en este disco.\n");
                        return;
                    }
                    }
                }
                else if((mbrRead.mbr_partition_4.part_status=='1')&&
                        ((mbrRead.mbr_partition_4.part_type=='e')||
                        (mbrRead.mbr_partition_4.part_type=='E'))){
                    ebr ebrR;
                    fseek(archivo,mbrRead.mbr_partition_4.part_start,SEEK_SET);
                    fread(&ebrR,sizeof(ebr),1,archivo);
                    while(ebrR.part_status!='0'){
                        if(strcmp(ebrR.part_name,n.name)==0){
                            printf("Error: El nombre de la particion %s ya existe en este disco.\n",n.name);
                            fclose(archivo);
                            return;
                        }
                        if(ebrR.part_next>0){
                            fseek(archivo,ebrR.part_next,SEEK_SET);
                            fread(&ebrR,sizeof(ebr),1,archivo);
                        }else break;
                        if (strcasecmp(n.type,"e")==0){
                        printf("Error: Ya existe una partición extendida en este disco.\n");
                        return;
                    }
                    }
                }


                if ((nPartition.part_type=='l')||(nPartition.part_type=='L')){
                    ebr ebrW;
                    strcpy(ebrW.part_fit,nPartition.part_fit);
                    strcpy(ebrW.part_name,nPartition.part_name);
                    ebrW.part_next=-1;
                    ebrW.part_size=n.size*mul;
                    ebrW.part_status = '1';
                    ebr ebrR;
                    if((mbrRead.mbr_partition_1.part_status=='1')&&
                            ((mbrRead.mbr_partition_1.part_type=='e')||
                            (mbrRead.mbr_partition_1.part_type=='E'))){
                                int tamanioActual=0;
                                int punteroEbr=0;
                                punteroEbr=mbrRead.mbr_partition_1.part_start;
                                fseek(archivo,punteroEbr,SEEK_SET);
                                fread(&ebrR,sizeof(ebr),1,archivo);
                                ebrW.part_start=mbrRead.mbr_partition_1.part_start;
                                while(ebrR.part_status=='1'){
                                    tamanioActual+=ebrR.part_size;
                                    ebrW.part_start=ebrW.part_start+ebrR.part_size;
                                   if(ebrR.part_next>0){
                                       punteroEbr=ebrR.part_next;

                                       fseek(archivo,ebrR.part_next,SEEK_SET);
                                       fread(&ebrR,sizeof(ebr),1,archivo);

                                   }else{
                                       punteroEbr=ebrR.part_start+ebrR.part_size;
                                       break;
                                   }

                                }
                                if(mbrRead.mbr_partition_1.part_size>=(tamanioActual+nPartition.part_size)){
                                    if(nPartition.part_size>sizeof(ebr)){
                                        if(punteroEbr!=mbrRead.mbr_partition_1.part_start){
                                            ebrR.part_next=punteroEbr;
                                            fseek(archivo,ebrR.part_start,SEEK_SET);
                                            fwrite(&ebrR,sizeof(ebr),1,archivo);
                                        }
                                        fseek(archivo,punteroEbr,SEEK_SET);
                                        fwrite(&ebrW,sizeof(ebr),1,archivo);
                                    }
                                    else{
                                        printf("Error: La particion logica es demasiado pequeña para"
                                                "contener archivos.\n");
                                        fclose(archivo);
                                        return;
                                    }
                                }else{
                                    printf("Error: La particion logica sobrepasa el "
                                                "tamaño de la particion extendida.\n");
                                    fclose(archivo);
                                    return;
                                }

                    }
                    else if((mbrRead.mbr_partition_2.part_status=='1')&&
                            ((mbrRead.mbr_partition_2.part_type=='e')||
                            (mbrRead.mbr_partition_2.part_type=='E'))){
                                int tamanioActual=0;
                                int punteroEbr=0;
                                punteroEbr=mbrRead.mbr_partition_2.part_start;
                                fseek(archivo,punteroEbr,SEEK_SET);
                                fread(&ebrR,sizeof(ebr),1,archivo);
                                ebrW.part_start=mbrRead.mbr_partition_2.part_start;
                                while(ebrR.part_status=='1'){
                                    tamanioActual+=ebrR.part_size;
                                    ebrW.part_start=ebrW.part_start+ebrR.part_size;
                                   if(ebrR.part_next>0){
                                       punteroEbr=ebrR.part_next;

                                       fseek(archivo,ebrR.part_next,SEEK_SET);
                                       fread(&ebrR,sizeof(ebr),1,archivo);
                                   }else{
                                       punteroEbr=ebrR.part_start+ebrR.part_size;
                                       break;
                                   }

                                }
                                if(mbrRead.mbr_partition_2.part_size>=(tamanioActual+nPartition.part_size)){
                                    if(nPartition.part_size>sizeof(ebr)){
                                        if(punteroEbr!=mbrRead.mbr_partition_2.part_start){
                                            ebrR.part_next=punteroEbr;
                                            fseek(archivo,ebrR.part_start,SEEK_SET);
                                            fwrite(&ebrR,sizeof(ebr),1,archivo);
                                        }
                                        fseek(archivo,punteroEbr,SEEK_SET);
                                        fwrite(&ebrW,sizeof(ebr),1,archivo);
                                    }
                                    else{
                                        printf("Error: La particion logica es demasiado pequeña para"
                                                "contener archivos.\n");
                                        fclose(archivo);
                                        return;
                                    }
                                }else{
                                    printf("Error: La particion logica sobrepasa el "
                                                "tamaño de la particion extendida.\n");
                                    fclose(archivo);
                                    return;
                                }

                    }


                    else if((mbrRead.mbr_partition_3.part_status=='1')&&
                            ((mbrRead.mbr_partition_3.part_type=='e')||
                            (mbrRead.mbr_partition_3.part_type=='E'))){
                                int tamanioActual=0;
                                int punteroEbr=0;
                                punteroEbr=mbrRead.mbr_partition_3.part_start;
                                fseek(archivo,punteroEbr,SEEK_SET);
                                fread(&ebrR,sizeof(ebr),1,archivo);
                                ebrW.part_start=mbrRead.mbr_partition_3.part_start;
                                while(ebrR.part_status=='1'){
                                    tamanioActual+=ebrR.part_size;
                                    ebrW.part_start=ebrW.part_start+ebrR.part_size;
                                   if(ebrR.part_next>0){
                                       punteroEbr=ebrR.part_next;
                                       fseek(archivo,ebrR.part_next,SEEK_SET);
                                       fread(&ebrR,sizeof(ebr),1,archivo);
                                   }else{
                                       punteroEbr=ebrR.part_start+ebrR.part_size;
                                       break;
                                   }

                                }
                                if(mbrRead.mbr_partition_3.part_size>=(tamanioActual+nPartition.part_size)){
                                    if(nPartition.part_size>sizeof(ebr)){
                                        if(punteroEbr!=mbrRead.mbr_partition_3.part_start){
                                            ebrR.part_next=punteroEbr;
                                            fseek(archivo,ebrR.part_start,SEEK_SET);
                                            fwrite(&ebrR,sizeof(ebr),1,archivo);
                                        }
                                        fseek(archivo,punteroEbr,SEEK_SET);
                                        fwrite(&ebrW,sizeof(ebr),1,archivo);
                                    }
                                    else{
                                        printf("Error: La particion logica es demasiado pequeña para"
                                                "contener archivos.\n");
                                        fclose(archivo);
                                        return;
                                    }
                                }else{
                                    printf("Error: La particion logica sobrepasa el "
                                                "tamaño de la particion extendida.\n");
                                    fclose(archivo);
                                    return;
                                }

                    }
                    else if((mbrRead.mbr_partition_4.part_status=='1')&&
                            ((mbrRead.mbr_partition_4.part_type=='e')||
                            (mbrRead.mbr_partition_4.part_type=='E'))){
                                int tamanioActual=0;
                                int punteroEbr=0;
                                punteroEbr=mbrRead.mbr_partition_4.part_start;
                                fseek(archivo,punteroEbr,SEEK_SET);
                                fread(&ebrR,sizeof(ebr),1,archivo);
                                ebrW.part_start=mbrRead.mbr_partition_4.part_start;
                                while(ebrR.part_status=='1'){
                                    tamanioActual+=ebrR.part_size;
                                    ebrW.part_start=ebrW.part_start+ebrR.part_size;
                                   if(ebrR.part_next>0){
                                       punteroEbr=ebrR.part_next;

                                       fseek(archivo,ebrR.part_next,SEEK_SET);
                                       fread(&ebrR,sizeof(ebr),1,archivo);
                                   }else{
                                       punteroEbr=ebrR.part_start+ebrR.part_size;
                                       break;
                                   }

                                }
                                if(mbrRead.mbr_partition_4.part_size>=(tamanioActual+nPartition.part_size)){
                                    if(nPartition.part_size>sizeof(ebr)){
                                        if(punteroEbr!=mbrRead.mbr_partition_4.part_start){
                                            ebrR.part_next=punteroEbr;
                                            fseek(archivo,ebrR.part_start,SEEK_SET);
                                            fwrite(&ebrR,sizeof(ebr),1,archivo);
                                        }
                                        fseek(archivo,punteroEbr,SEEK_SET);
                                        fwrite(&ebrW,sizeof(ebr),1,archivo);
                                    }
                                    else{
                                        printf("Error: La particion logica es demasiado pequeña para"
                                                "contener archivos.\n");
                                        fclose(archivo);
                                        return;
                                    }
                                }else{
                                    printf("Error: La particion logica sobrepasa el "
                                                "tamaño de la particion extendida.\n");
                                    fclose(archivo);
                                    return;
                                }

                    }else{
                        printf("Error: No existe particion extendida para crear la particion logica.\n");
                        fclose(archivo);
                        return;
                    }

                }else{
                    if(mbrRead.mbr_partition_1.part_status=='0'){
                        mbrRead.mbr_partition_1=nPartition;
                        nPartition.part_status='1';
                        nPartition.part_start=sizeof(mbr);
                        mbrRead.mbr_partition_1.part_start=sizeof(mbr);
                        fseek(archivo,0,SEEK_SET);
                        fwrite(&mbrRead,sizeof(mbr),1,archivo);
                        if((nPartition.part_type=='e')||(nPartition.part_type=='E')){
                            ebr nebr;
                            nebr.part_status='0';
                            nebr.part_next=-1;
                            nebr.part_size=0;
                            fseek(archivo,mbrRead.mbr_partition_1.part_start,SEEK_SET);
                            fwrite(&nebr,sizeof(ebr),1,archivo);
                        }
                    }
                    else if(mbrRead.mbr_partition_2.part_status=='0'){
                        mbrRead.mbr_partition_2=nPartition;
                        nPartition.part_status='1';
                        nPartition.part_start=sizeof(mbr);
                        mbrRead.mbr_partition_2.part_start=mbrRead.mbr_partition_1.part_start+
                                mbrRead.mbr_partition_1.part_size;
                        fseek(archivo,0,SEEK_SET);
                        fwrite(&mbrRead,sizeof(mbr),1,archivo);
                        if((nPartition.part_type=='e')||(nPartition.part_type=='E')){
                            ebr nebr;
                            nebr.part_status='0';
                            nebr.part_next=-1;
                            nebr.part_size=0;
                            fseek(archivo,mbrRead.mbr_partition_2.part_start,SEEK_SET);
                            fwrite(&nebr,sizeof(ebr),1,archivo);
                        }
                    }
                    else if(mbrRead.mbr_partition_3.part_status=='0'){
                        mbrRead.mbr_partition_3=nPartition;
                        nPartition.part_status='1';
                        nPartition.part_start=sizeof(mbr);
                        mbrRead.mbr_partition_3.part_start=mbrRead.mbr_partition_2.part_start+
                                mbrRead.mbr_partition_2.part_size;
                        fseek(archivo,0,SEEK_SET);
                        fwrite(&mbrRead,sizeof(mbr),1,archivo);
                        if((nPartition.part_type=='e')||(nPartition.part_type=='E')){
                            ebr nebr;
                            nebr.part_status='0';
                            nebr.part_next=-1;
                            nebr.part_size=0;
                            fseek(archivo,mbrRead.mbr_partition_3.part_start,SEEK_SET);
                            fwrite(&nebr,sizeof(ebr),1,archivo);
                        }
                    }
                    else if(mbrRead.mbr_partition_4.part_status=='0'){
                        mbrRead.mbr_partition_4=nPartition;
                        nPartition.part_status='1';
                        nPartition.part_start=sizeof(mbr);
                        mbrRead.mbr_partition_4.part_start=mbrRead.mbr_partition_3.part_start+
                                mbrRead.mbr_partition_3.part_size;
                        fseek(archivo,0,SEEK_SET);
                        fwrite(&mbrRead,sizeof(mbr),1,archivo);
                        if((nPartition.part_type=='e')||(nPartition.part_type=='E')){
                            ebr nebr;
                            nebr.part_status='0';
                            nebr.part_next=-1;
                            nebr.part_size=0;
                            fseek(archivo,mbrRead.mbr_partition_4.part_start,SEEK_SET);
                            fwrite(&nebr,sizeof(ebr),1,archivo);
                        }
                    }
                    else{
                        printf("Las particiones del disco estan llenas.\n");
                        fclose(archivo);
                        return;
                    }
                    fseek(archivo,0,SEEK_SET);
                    fwrite(&mbrRead,sizeof(mbr),1,archivo);
                }

                fclose(archivo);
                printf("Particion %s creada correctamente. \n",n.name);
                return;
            }else{
                printf("Problema al abrir el archivo %s. \n",n.path);
                return;
            }
        }else{
            printf("El atributo -size tiene un valor menor o igual a 0: %d \n", n.size);
            return;
        }
    }else if(tipoFDISK==2){
        if(n.size==0)
        {printf("No se modifica la particion porque -size tiene valor de 0.\n");
        return;}
        if (n.unit=='k')
                mul =1024;
            else if (n.unit=='K')
                mul =1024;
            else if(n.unit=='m')
                mul = 1024*1024;
            else if(n.unit=='M')
                mul = 1024*1024;
            else if(n.unit=='b')
                mul = 1;
            else if(n.unit=='B')
                mul = 1;
            else
            {printf("Error en atributo -unit: %c\n",n.unit); return;}



    }else if(tipoFDISK==1){
        FILE* arch = fopen(n.path,"rb+");
        if(arch){
        mbr mbrRead;
        partition part;
        strcpy(part.part_name,n.name);
        fseek(arch,0,SEEK_SET);
        fread(&mbrRead,sizeof(mbr),1,arch);

        if(existeNombreParticionPrimariaExtendida(mbrRead,part)==0){
            if((mbrRead.mbr_partition_1.part_status=='1')&&
                        ((mbrRead.mbr_partition_1.part_type=='e')||
                        (mbrRead.mbr_partition_1.part_type=='E'))){
                    ebr ebrR;
                    fseek(arch,mbrRead.mbr_partition_1.part_start,SEEK_SET);
                    fread(&ebrR,sizeof(ebr),1,arch);
                    while(ebrR.part_status!='0'){
                        if(strcmp(ebrR.part_name,n.name)==0){
                            fclose(arch);
                            goto siguientePaso;
                        }
                        if(ebrR.part_next>0){
                            fseek(arch,ebrR.part_next,SEEK_SET);
                            fread(&ebrR,sizeof(ebr),1,arch);
                        }else break;
                    }
                    if (mbrRead.mbr_partition_1.part_type=='e'||mbrRead.mbr_partition_1.part_type=='E'){
                        printf("La particion %s no existe en el disco.\n",n.path);
                        return;
                    }
                }
                else if((mbrRead.mbr_partition_2.part_status=='1')&&
                        ((mbrRead.mbr_partition_2.part_type=='e')||
                        (mbrRead.mbr_partition_2.part_type=='E'))){
                    ebr ebrR;
                    fseek(arch,mbrRead.mbr_partition_2.part_start,SEEK_SET);
                    fread(&ebrR,sizeof(ebr),1,arch);
                    while(ebrR.part_status!='0'){
                        if(strcmp(ebrR.part_name,n.name)==0){
                            fclose(arch);
                            goto siguientePaso;
                        }
                        if(ebrR.part_next>0){
                            fseek(arch,ebrR.part_next,SEEK_SET);
                            fread(&ebrR,sizeof(ebr),1,arch);
                        }else break;
                    }
                    if (mbrRead.mbr_partition_2.part_type=='e'||mbrRead.mbr_partition_2.part_type=='E'){
                        printf("La particion %s no existe en el disco.\n",n.path);
                        return;
                    }
                }
                else if((mbrRead.mbr_partition_3.part_status=='1')&&
                        ((mbrRead.mbr_partition_3.part_type=='e')||
                        (mbrRead.mbr_partition_3.part_type=='E'))){
                    ebr ebrR;
                    fseek(arch,mbrRead.mbr_partition_3.part_start,SEEK_SET);
                    fread(&ebrR,sizeof(ebr),1,arch);
                    while(ebrR.part_status!='0'){
                        if(strcmp(ebrR.part_name,n.name)==0){
                            fclose(arch);
                            goto siguientePaso;
                        }
                        if(ebrR.part_next>0){
                            fseek(arch,ebrR.part_next,SEEK_SET);
                            fread(&ebrR,sizeof(ebr),1,arch);
                        }else break;
                    }
                    if (mbrRead.mbr_partition_3.part_type=='e'||mbrRead.mbr_partition_3.part_type=='E'){
                        printf("La particion %s no existe en el disco.\n",n.path);
                        return;
                    }
                }
                else if((mbrRead.mbr_partition_4.part_status=='1')&&
                        ((mbrRead.mbr_partition_4.part_type=='e')||
                        (mbrRead.mbr_partition_4.part_type=='E'))){
                    ebr ebrR;
                    fseek(arch,mbrRead.mbr_partition_4.part_start,SEEK_SET);
                    fread(&ebrR,sizeof(ebr),1,arch);
                    while(ebrR.part_status!='0'){
                        if(strcmp(ebrR.part_name,n.name)==0){
                            fclose(arch);
                            goto siguientePaso;
                        }
                        if(ebrR.part_next>0){
                            fseek(arch,ebrR.part_next,SEEK_SET);
                            fread(&ebrR,sizeof(ebr),1,arch);
                        }else break;
                    }
                    if (mbrRead.mbr_partition_4.part_type=='e'||mbrRead.mbr_partition_4.part_type=='E'){
                        printf("La particion %s no existe en el disco.\n",n.path);
                        return;
                    }
                }
        }


        fclose(arch);
    }
    else{
        printf("El disco %s no existe.\n",n.path);
        return;
    }

        siguientePaso:
        if (!((strcasecmp(n.deleteAtr,"fast")==0)||(strcasecmp(n.deleteAtr,"full")==0)))
            {printf("Error en atributo -delete: %s\n",n.deleteAtr); return;}

        FILE *archivo=fopen(n.path,"rb+");
            if(archivo)
            {
                mbr mbrRead;
                fseek(archivo,0,SEEK_SET);
                fread(&mbrRead,sizeof(mbr),1,archivo);

                    if(mbrRead.mbr_partition_1.part_status=='1'){
                        if(strcmp(mbrRead.mbr_partition_1.part_name,n.name)==0){
                            partition ntmp;
                            ntmp.part_status='0';
                            mbrRead.mbr_partition_1.part_status='0';
                            fseek(archivo,0,SEEK_SET);
                            fwrite(&mbrRead,sizeof(mbr),1,archivo);
                            if(strcasecmp(n.deleteAtr,"full")==0){

                                int tamanio =  mbrRead.mbr_partition_1.part_size;
                                fseek(archivo,mbrRead.mbr_partition_1.part_start,SEEK_SET);
                                const char xx[2] = "\0";
                                fwrite(xx,1,tamanio,archivo);
                            }
                            printf("particion %s borrada.\n",n.name);
                            fclose(archivo);
                            return;
                        }
                        else if((mbrRead.mbr_partition_1.part_type=='e')||
                                (mbrRead.mbr_partition_1.part_type=='E')){

                            ebr rebr;
                            int puntebr;
                            fseek(archivo,mbrRead.mbr_partition_1.part_start,SEEK_SET);
                            //fseek(archivo,sizeof(ebr),SEEK_CUR);


                            puntebr = ftell(archivo);
                            fread(&rebr,sizeof(ebr),1,archivo);
                            if(rebr.part_status=='1'){
                                if(strcmp(rebr.part_name,n.name)==0){
                                    int tfin = rebr.part_size;
                                        int ini =  rebr.part_start;
                                        const char xx[2]="\0";
                                    if(rebr.part_next!=-1){
                                        mbrRead.mbr_partition_1.part_start=rebr.part_next;
                                        fseek(archivo,0,SEEK_SET);
                                        fwrite(&mbrRead,sizeof(mbr),1,archivo);

                                    }else{
                                        rebr.part_status='0';
                                        rebr.part_next=-1;
                                        strcpy(rebr.part_name,NULL);

                                        fseek(archivo,rebr.part_start,SEEK_SET);
                                        fwrite(&rebr,sizeof(ebr),1,archivo);
                                    }
                                    if(strcasecmp(n.type,"full")){
                                            fseek(archivo,ini+sizeof(ebr),SEEK_SET);
                                            fwrite(xx,1,tfin,archivo);
                                        }
                                        printf("particion %s borrada.\n",n.name);
                                        fclose(archivo);
                                        return;

                                    }
                                else{
                                    if(rebr.part_next!=-1){
                                        const char xx[2] = "/0";
                                        ebr xebr;
                                        fseek(archivo,rebr.part_next,SEEK_SET);
                                        fread(&xebr,sizeof(ebr),1,archivo);
                                        if(strcmp(xebr.part_name,n.name)==0){
                                            rebr.part_next=xebr.part_next;
                                            fseek(archivo,rebr.part_start,SEEK_SET);
                                            fwrite(&rebr,sizeof(ebr),1,archivo);
                                            if(strcmp(n.type,"full")){
                                                fseek(archivo,xebr.part_start,SEEK_SET);
                                                fwrite(xx,1,xebr.part_size,archivo);
                                            }
                                            printf("particion %s borrada.\n",n.name);
                                            fclose(archivo);
                                            return;
                                        }else{
                                            while(xebr.part_next!=-1){
                                                rebr=xebr;
                                                fseek(archivo,xebr.part_next,SEEK_SET);
                                                fread(&xebr,sizeof(ebr),1,archivo);
                                                if(strcmp(xebr.part_name,n.name)==0){
                                                    rebr.part_next=xebr.part_next;
                                                    fseek(archivo,rebr.part_start,SEEK_SET);
                                                    fwrite(&rebr,sizeof(ebr),1,archivo);
                                                    if(strcmp(n.type,"full")){
                                                        fseek(archivo,xebr.part_start,SEEK_SET);
                                                        fwrite(xx,1,xebr.part_size,archivo);
                                                    }
                                                    printf("particion %s borrada.\n",n.name);
                                                    fclose(archivo);
                                                    return;
                                        }
                                            }
                                        }

                                    }

                                }
                                }

                            }
                        }
                //!!!!empieza part2

                if(mbrRead.mbr_partition_2.part_status=='1'){
                        if(strcmp(mbrRead.mbr_partition_2.part_name,n.name)==0){
                            partition ntmp;
                            ntmp.part_status='0';
                            mbrRead.mbr_partition_2.part_status='0';
                            fseek(archivo,0,SEEK_SET);
                            fwrite(&mbrRead,sizeof(mbr),1,archivo);
                            if(strcasecmp(n.deleteAtr,"full")==0){

                                int tamanio =  mbrRead.mbr_partition_2.part_size;
                                fseek(archivo,mbrRead.mbr_partition_2.part_start,SEEK_SET);
                                const char xx[2] = "\0";
                                fwrite(xx,1,tamanio,archivo);
                            }
                            printf("particion %s borrada.\n",n.name);
                            fclose(archivo);
                            return;
                        }
                        else if((mbrRead.mbr_partition_2.part_type=='e')||
                                (mbrRead.mbr_partition_2.part_type=='E')){

                            ebr rebr;
                            int puntebr;
                            fseek(archivo,mbrRead.mbr_partition_2.part_start,SEEK_SET);
                            //fseek(archivo,sizeof(ebr),SEEK_CUR);


                            puntebr = ftell(archivo);
                            fread(&rebr,sizeof(ebr),1,archivo);
                            if(rebr.part_status=='1'){
                                if(strcmp(rebr.part_name,n.name)==0){
                                    int tfin = rebr.part_size;
                                        int ini =  rebr.part_start;
                                        const char xx[2]="\0";
                                    if(rebr.part_next!=-1){
                                        mbrRead.mbr_partition_2.part_start=rebr.part_next;
                                        fseek(archivo,0,SEEK_SET);
                                        fwrite(&mbrRead,sizeof(mbr),1,archivo);

                                    }else{
                                        rebr.part_status='0';
                                        rebr.part_next=-1;
                                        strcpy(rebr.part_name,NULL);
                                        fseek(archivo,rebr.part_start,SEEK_SET);
                                        fwrite(&rebr,sizeof(ebr),1,archivo);
                                    }
                                    if(strcasecmp(n.type,"full")){
                                            fseek(archivo,ini+sizeof(ebr),SEEK_SET);
                                            fwrite(xx,1,tfin,archivo);
                                        }
                                        printf("particion %s borrada.\n",n.name);
                                        fclose(archivo);
                                        return;

                                    }
                                else{
                                    if(rebr.part_next!=-1){
                                        const char xx[2] = "/0";
                                        ebr xebr;
                                        fseek(archivo,rebr.part_next,SEEK_SET);
                                        fread(&xebr,sizeof(ebr),1,archivo);
                                        if(strcmp(xebr.part_name,n.name)==0){
                                            rebr.part_next=xebr.part_next;
                                            fseek(archivo,rebr.part_start,SEEK_SET);
                                            fwrite(&rebr,sizeof(ebr),1,archivo);
                                            if(strcmp(n.type,"full")){
                                                fseek(archivo,xebr.part_start,SEEK_SET);
                                                fwrite(xx,1,xebr.part_size,archivo);
                                            }
                                            printf("particion %s borrada.\n",n.name);
                                            fclose(archivo);
                                            return;
                                        }else{
                                            while(xebr.part_next!=-1){
                                                rebr=xebr;
                                                fseek(archivo,xebr.part_next,SEEK_SET);
                                                fread(&xebr,sizeof(ebr),1,archivo);
                                                if(strcmp(xebr.part_name,n.name)==0){
                                                    rebr.part_next=xebr.part_next;
                                                    fseek(archivo,rebr.part_start,SEEK_SET);
                                                    fwrite(&rebr,sizeof(ebr),1,archivo);
                                                    if(strcmp(n.type,"full")){
                                                        fseek(archivo,xebr.part_start,SEEK_SET);
                                                        fwrite(xx,1,xebr.part_size,archivo);
                                                    }
                                                    printf("particion %s borrada.\n",n.name);
                                                    fclose(archivo);
                                                    return;
                                        }
                                            }
                                        }

                                    }

                                }
                                }

                            }
                        }

                //!!!empieza part3
                if(mbrRead.mbr_partition_3.part_status=='1'){
                        if(strcmp(mbrRead.mbr_partition_3.part_name,n.name)==0){
                            partition ntmp;
                            ntmp.part_status='0';
                            mbrRead.mbr_partition_3.part_status='0';
                            fseek(archivo,0,SEEK_SET);
                            fwrite(&mbrRead,sizeof(mbr),1,archivo);
                            if(strcasecmp(n.deleteAtr,"full")==0){

                                int tamanio =  mbrRead.mbr_partition_3.part_size;
                                fseek(archivo,mbrRead.mbr_partition_3.part_start,SEEK_SET);
                                const char xx[2] = "\0";
                                fwrite(xx,1,tamanio,archivo);
                            }
                            printf("particion %s borrada.\n",n.name);
                            fclose(archivo);
                            return;
                        }
                        else if(((mbrRead.mbr_partition_3.part_type=='e')||
                                (mbrRead.mbr_partition_3.part_type=='E'))){

                            ebr rebr;
                            int puntebr;
                            fseek(archivo,mbrRead.mbr_partition_3.part_start,SEEK_SET);
                            //fseek(archivo,sizeof(ebr),SEEK_CUR);


                            puntebr = ftell(archivo);
                            fread(&rebr,sizeof(ebr),1,archivo);
                            if(rebr.part_status=='1'){
                                if(strcmp(rebr.part_name,n.name)==0){
                                    int tfin = rebr.part_size;
                                        int ini =  rebr.part_start;
                                        const char xx[2]="\0";
                                    if(rebr.part_next!=-1){
                                        mbrRead.mbr_partition_3.part_start=rebr.part_next;
                                        fseek(archivo,0,SEEK_SET);
                                        fwrite(&mbrRead,sizeof(mbr),1,archivo);

                                    }else{
                                        rebr.part_status='0';
                                        rebr.part_next=-1;
                                        strcpy(rebr.part_name,NULL);
                                        fseek(archivo,rebr.part_start,SEEK_SET);
                                        fwrite(&rebr,sizeof(ebr),1,archivo);
                                    }
                                    if(strcasecmp(n.type,"full")){
                                            fseek(archivo,ini+sizeof(ebr),SEEK_SET);
                                            fwrite(xx,1,tfin,archivo);
                                        }
                                        printf("particion %s borrada.\n",n.name);
                                        fclose(archivo);
                                        return;

                                    }
                                else{
                                    if(rebr.part_next!=-1){
                                        const char xx[2] = "0";
                                        ebr xebr;
                                        fseek(archivo,rebr.part_next,SEEK_SET);
                                        fread(&xebr,sizeof(ebr),1,archivo);
                                        if(strcmp(xebr.part_name,n.name)==0){
                                            rebr.part_next=xebr.part_next;
                                            fseek(archivo,rebr.part_start,SEEK_SET);
                                            fwrite(&rebr,sizeof(ebr),1,archivo);
                                            if(strcmp(n.type,"full")){
                                                fseek(archivo,xebr.part_start,SEEK_SET);
                                                fwrite(xx,1,xebr.part_size,archivo);
                                            }
                                            printf("particion %s borrada.\n",n.name);
                                            fclose(archivo);
                                            return;
                                        }else{
                                            while(xebr.part_next!=-1){
                                                rebr=xebr;
                                                fseek(archivo,xebr.part_next,SEEK_SET);
                                                fread(&xebr,sizeof(ebr),1,archivo);
                                                if(strcmp(xebr.part_name,n.name)==0){
                                                    rebr.part_next=xebr.part_next;
                                                    fseek(archivo,rebr.part_start,SEEK_SET);
                                                    fwrite(&rebr,sizeof(ebr),1,archivo);
                                                    if(strcmp(n.type,"full")){
                                                        fseek(archivo,xebr.part_start,SEEK_SET);
                                                        fwrite(xx,1,xebr.part_size,archivo);
                                                    }
                                                    printf("particion %s borrada.\n",n.name);
                                                    fclose(archivo);
                                                    return;
                                        }
                                            }
                                        }

                                    }

                                }
                                }

                            }
                        }

                //!!empieza part4
                if(mbrRead.mbr_partition_4.part_status=='1'){
                        if(strcmp(mbrRead.mbr_partition_4.part_name,n.name)==0){
                            partition ntmp;
                            ntmp.part_status='0';
                            mbrRead.mbr_partition_4.part_status='0';
                            fseek(archivo,0,SEEK_SET);
                            fwrite(&mbrRead,sizeof(mbr),1,archivo);
                            if(strcasecmp(n.deleteAtr,"full")==0){

                                int tamanio =  mbrRead.mbr_partition_4.part_size;
                                fseek(archivo,mbrRead.mbr_partition_4.part_start,SEEK_SET);
                                const char xx[2] = "\0";
                                fwrite(xx,1,tamanio,archivo);
                            }
                            printf("particion %s borrada.\n",n.name);
                            fclose(archivo);
                            return;
                        }
                        else if((mbrRead.mbr_partition_4.part_type=='e')||
                                (mbrRead.mbr_partition_4.part_type=='E')){

                            ebr rebr;
                            int puntebr;
                            fseek(archivo,mbrRead.mbr_partition_4.part_start,SEEK_SET);
                            //fseek(archivo,sizeof(ebr),SEEK_CUR);


                            puntebr = ftell(archivo);
                            fread(&rebr,sizeof(ebr),1,archivo);
                            if(rebr.part_status=='1'){
                                if(strcmp(rebr.part_name,n.name)==0){
                                    int tfin = rebr.part_size;
                                        int ini =  rebr.part_start;
                                        const char xx[2]="0";
                                    if(rebr.part_next!=-1){
                                        mbrRead.mbr_partition_4.part_start=rebr.part_next;
                                        fseek(archivo,0,SEEK_SET);
                                        fwrite(&mbrRead,sizeof(mbr),1,archivo);

                                    }else{
                                        rebr.part_status='0';
                                        rebr.part_next=-1;
                                        strcpy(rebr.part_name,NULL);
                                        fseek(archivo,rebr.part_start,SEEK_SET);
                                        fwrite(&rebr,sizeof(ebr),1,archivo);
                                    }
                                    if(strcasecmp(n.type,"full")){
                                            fseek(archivo,ini+sizeof(ebr),SEEK_SET);
                                            fwrite(xx,1,tfin,archivo);
                                        }
                                        printf("particion %s borrada.\n",n.name);
                                        fclose(archivo);
                                        return;

                                    }
                                else{
                                    if(rebr.part_next!=-1){
                                        const char xx[2] = "/0";
                                        ebr xebr;
                                        fseek(archivo,rebr.part_next,SEEK_SET);
                                        fread(&xebr,sizeof(ebr),1,archivo);
                                        if(strcmp(xebr.part_name,n.name)==0){
                                            rebr.part_next=xebr.part_next;
                                            fseek(archivo,rebr.part_start,SEEK_SET);
                                            fwrite(&rebr,sizeof(ebr),1,archivo);
                                            if(strcmp(n.type,"full")){
                                                fseek(archivo,xebr.part_start,SEEK_SET);
                                                fwrite(xx,1,xebr.part_size,archivo);
                                            }
                                            printf("particion %s borrada.\n",n.name);
                                            fclose(archivo);
                                            return;
                                        }else{
                                            while(xebr.part_next!=-1){
                                                rebr=xebr;
                                                fseek(archivo,xebr.part_next,SEEK_SET);
                                                fread(&xebr,sizeof(ebr),1,archivo);
                                                if(strcmp(xebr.part_name,n.name)==0){
                                                    rebr.part_next=xebr.part_next;
                                                    fseek(archivo,rebr.part_start,SEEK_SET);
                                                    fwrite(&rebr,sizeof(ebr),1,archivo);
                                                    if(strcmp(n.type,"full")){
                                                        fseek(archivo,xebr.part_start,SEEK_SET);
                                                        fwrite(xx,1,xebr.part_size,archivo);
                                                    }
                                                    printf("particion %s borrada.\n",n.name);
                                                    fclose(archivo);
                                                    return;
                                        }
                                            }
                                        }

                                    }

                                }
                                }

                            }
                        }

                fclose(archivo);
            }
    }
}
void listarMount(){
     if(montados==NULL)
    {
        printf("No hay discos ni particiones montadas.\n");
    }else{
        nodoDisco* discoActual;
        discoActual = montados->inicio;
        while(discoActual!=NULL){
            nodoParticion* partActual;
            partActual = discoActual->inicio;
            while(partActual!=NULL){
                printf("id::%s -path::\"%s\" -name::\"%s\"\n",partActual->idParticion,discoActual->path,
                partActual->name);
                partActual=partActual->siguiente;
            }
            discoActual = discoActual->siguiente;
        }
    }
}

void mount(atributeStruct n){
    nodoDisco* actual;
    nodoDisco* nuevo = (nodoDisco*)malloc(sizeof(nodoDisco));
    const char vd[3]="vd";
    char* id=(char*)malloc(sizeof(char)*4);
    strcpy(id,vd);
    char letra = 'a';
    id[2]=letra;
    nuevo->inicio=NULL;
    nuevo->siguiente=NULL;
    nuevo->particiones=0;
    nuevo->idDisco=(char*)malloc(sizeof(char)*4);
    FILE* arch =fopen(n.path,"rb+");
    if(arch){
        mbr mbrRead;
        partition part;
        strcpy(part.part_name,n.name);
        fseek(arch,0,SEEK_SET);
        fread(&mbrRead,sizeof(mbr),1,arch);

        if(existeNombreParticionPrimariaExtendida(mbrRead,part)==0){
            if((mbrRead.mbr_partition_1.part_status=='1')&&
                        ((mbrRead.mbr_partition_1.part_type=='e')||
                        (mbrRead.mbr_partition_1.part_type=='E'))){
                    ebr ebrR;
                    fseek(arch,mbrRead.mbr_partition_1.part_start,SEEK_SET);
                    fread(&ebrR,sizeof(ebr),1,arch);
                    while(ebrR.part_status!='0'){
                        if(strcmp(ebrR.part_name,n.name)==0){
                            fclose(arch);
                            goto siguientePaso;
                        }
                        if(ebrR.part_next>0){
                            fseek(arch,ebrR.part_next,SEEK_SET);
                            fread(&ebrR,sizeof(ebr),1,arch);
                        }else break;
                    }
                    if (mbrRead.mbr_partition_1.part_type=='e'||mbrRead.mbr_partition_1.part_type=='E'){
                        printf("La particion %s no existe en el disco.\n",n.path);
                        return;
                    }
                }
                else if((mbrRead.mbr_partition_2.part_status=='1')&&
                        ((mbrRead.mbr_partition_2.part_type=='e')||
                        (mbrRead.mbr_partition_2.part_type=='E'))){
                    ebr ebrR;
                    fseek(arch,mbrRead.mbr_partition_2.part_start,SEEK_SET);
                    fread(&ebrR,sizeof(ebr),1,arch);
                    while(ebrR.part_status!='0'){
                        if(strcmp(ebrR.part_name,n.name)==0){
                            fclose(arch);
                            goto siguientePaso;
                        }
                        if(ebrR.part_next>0){
                            fseek(arch,ebrR.part_next,SEEK_SET);
                            fread(&ebrR,sizeof(ebr),1,arch);
                        }else break;
                    }
                    if (mbrRead.mbr_partition_2.part_type=='e'||mbrRead.mbr_partition_2.part_type=='E'){
                        printf("La particion %s no existe en el disco.\n",n.path);
                        return;
                    }
                }
                else if((mbrRead.mbr_partition_3.part_status=='1')&&
                        ((mbrRead.mbr_partition_3.part_type=='e')||
                        (mbrRead.mbr_partition_3.part_type=='E'))){
                    ebr ebrR;
                    fseek(arch,mbrRead.mbr_partition_3.part_start,SEEK_SET);
                    fread(&ebrR,sizeof(ebr),1,arch);
                    while(ebrR.part_status!='0'){
                        if(strcmp(ebrR.part_name,n.name)==0){
                            fclose(arch);
                            goto siguientePaso;
                        }
                        if(ebrR.part_next>0){
                            fseek(arch,ebrR.part_next,SEEK_SET);
                            fread(&ebrR,sizeof(ebr),1,arch);
                        }else break;
                    }
                    if (mbrRead.mbr_partition_3.part_type=='e'||mbrRead.mbr_partition_3.part_type=='E'){
                        printf("La particion %s no existe en el disco.\n",n.path);
                        return;
                    }
                }
                else if((mbrRead.mbr_partition_4.part_status=='1')&&
                        ((mbrRead.mbr_partition_4.part_type=='e')||
                        (mbrRead.mbr_partition_4.part_type=='E'))){
                    ebr ebrR;
                    fseek(arch,mbrRead.mbr_partition_4.part_start,SEEK_SET);
                    fread(&ebrR,sizeof(ebr),1,arch);
                    while(ebrR.part_status!='0'){
                        if(strcmp(ebrR.part_name,n.name)==0){
                            fclose(arch);
                            goto siguientePaso;
                        }
                        if(ebrR.part_next>0){
                            fseek(arch,ebrR.part_next,SEEK_SET);
                            fread(&ebrR,sizeof(ebr),1,arch);
                        }else break;
                    }
                    if (mbrRead.mbr_partition_4.part_type=='e'||mbrRead.mbr_partition_4.part_type=='E'){
                        printf("La particion %s no existe en el disco.\n",n.path);
                        return;
                    }
                }
        }


        fclose(arch);
    }
    else{
        printf("El disco %s no existe.\n",n.path);
        return;
    }



    siguientePaso:

    if(montados==NULL)
    {
        montados = (nodoDisco*)malloc(sizeof(nodoDisco));
        strcpy(nuevo->idDisco,id);
        nuevo->letra=letra;
        nuevo->path = (char*)malloc(100);
        strcpy(nuevo->path,n.path);

        montados->discos=1;


        montados->inicio=nuevo;
        actual = montados->inicio;
    }
    else if(existeDiscoMontado(n.path)==0){
        while(existeIdDiscoMontado(letra)==1){
            letra+=1;
        }
        id[2]=letra;
        strcpy(nuevo->idDisco,id);
        nuevo->letra=letra;
        nuevo->path = (char*)malloc(100);
        strcpy(nuevo->path,n.path);
        montados->discos+=1;
        nodoDisco* ult = ultimoDiscoMontado();
        ult->siguiente=nuevo;
        actual = ult->siguiente;
    }else{
        actual = getDiscoMontado(n.path);
    }
    nodoParticion* nuevaP=(nodoParticion*)malloc(sizeof(nodoParticion));
    nuevaP->id=1;
    nuevaP->idParticion = (char*)malloc(sizeof(char)*5);
    nuevaP->name = (char*)malloc(sizeof(char)*17);
    strcpy(nuevaP->name,n.name);
    strcpy(nuevaP->idParticion,actual->idDisco);
    nuevaP->idParticion[3]=(nuevaP->id+48);
    nuevaP->siguiente=NULL;
    if (actual->inicio==NULL){
        //actual->inicio;//=(nodoParticion*)malloc(sizeof(nodoParticion));
        actual->inicio=nuevaP;
        actual->particiones=1;

        printf("La particion se ha montado correctamente. Id: %s.\n",nuevaP->idParticion);
    }
    else if(getParticionMontadaName(actual,nuevaP->name)==NULL){
        while(existeParticionMontada(actual,nuevaP->id)==1){
            nuevaP->id+=1;
        }
        actual->particiones+=1;
        nuevaP->idParticion[3]=(nuevaP->id+48);
        //ultimaParticionMontada(actual)->siguiente = (nodoParticion*)malloc(sizeof(nodoParticion));
        ultimaParticionMontada(actual)->siguiente=nuevaP;

        printf("La particion se ha montado correctamente. Id: %s.\n",nuevaP->idParticion);

    }else{
        printf("La particion ya ha sido montada.\n");
        return;
    }


}
void desmontar(nodoParticion* n,char letra){
    nodoDisco* dk =getIdDiscoMontado(letra);
    if(dk!=NULL){
        nodoParticion* piv;
        piv= dk->inicio;
        if(piv==n){
            dk->inicio = n->siguiente;
            dk->particiones--;
        }else{
            while((piv->siguiente!=NULL)){
                if(piv->siguiente == n){
                    piv->siguiente = n->siguiente;
                    dk->particiones--;
                }else
                piv=piv->siguiente;
            }
        }
        printf("Se ha desmontado la particion %s.\n",n->idParticion);
        if(dk->particiones==0)
            desmontar(dk,letra);
    }else{printf("No se ha podido desmontar la particion.\n"); return;}
    return;
}
void umount(atributeStruct n){
    nodoParticion* busq = existeParticionMontadaId(n.id);
    if(busq!=NULL){
        //char* bs = separarLetraIdDisco(n.id);
        nodoDisco* disk = getIdDiscoMontado(n.id[2]);

        desmontar(busq,n.id[2]);
    }else{
        printf("La particion %s no ha sido montada previamente.\n",n.id);
        return;
    }
}
void exec(atributeStruct n){
    FILE* arch;

    arch= fopen(n.path,"r");
       if( arch )   {
          int size = 1024, pos;
          int c;
          char *buffer = (char*)malloc(size);
          char* tk;
          char* cbuf;
          deNuevo:
          pos=0;
          buffer = (char*)malloc(size);
          do{ // read one line
          c = fgetc(arch);
          if(c != EOF) buffer[pos++] = (char)c;
          if(pos >= size - 1) { // increase buffer length - leave room for 0
            size *=2;
            buffer = (char*)realloc(buffer, size);
          }
        }while(c != EOF && c != '\n');
            buffer[pos++]='\0';

        //aqui va la leer()
        leer(buffer);


        getc(stdin);
        //printf("linea: %s \n",buffer);
        //free(buffer);
        if(c=='\n')
            goto deNuevo;
           fclose(arch);
     }
   else   printf( "El archivo %s no existe\n", n.path);
   return;
}

void reporteMBR(atributeStruct n){
    char* result = (char*)malloc(100);
    char* ar= (char*)malloc(20);
    strcpy(ar,"/home/mia/p.html");
    nodoDisco* disk = getIdDiscoMontado(n.id[2]);
    if(disk!=NULL){
        if(disk->particiones==0){
            printf("Disco no montado.\n");

        }
            FILE* report = fopen(n.path,"w+");
        if(report){
            strcpy(result,"<HTML><body>MBR \"");
            fwrite(result,17,1,report);
            fwrite(disk->path,strlen(disk->path),1,report);
            strcpy(result,"\"");
            fwrite(result,1,1,report);
            FILE* piv = fopen(disk->path,"rb+");
            if(piv){
                mbr mbrRead;
                partition nPartition;
                fseek(piv,0,SEEK_SET);
                fread(&mbrRead,sizeof(mbr),1,piv);
                strcpy(result,"<table border=1>\n<tr><td>Nombre<td/><td>Valor</td></tr>\n");
                fwrite(result,56,1,report);
                fprintf(report,"<tr><td>mbr_tamanio<td/><td>%d</td></tr>\n",mbrRead.mbr_tamanio);
                fprintf(report,"<tr><td>mbr_fecha_creacion<td/><td>%s</td></tr>\n",fecha(mbrRead.mbr_fecha_creacion));
                fprintf(report,"<tr><td>mbr_disk_signature<td/><td>%d</td></tr>\n",mbrRead.mbr_disk_signature);

                if(mbrRead.mbr_partition_1.part_status=='1'){
                    fprintf(report,"<tr><td>part_status_1<td/><td>%c</td></tr>\n",mbrRead.mbr_partition_1.part_status);
                    fprintf(report,"<tr><td>part_type_1<td/><td>%c</td></tr>\n",mbrRead.mbr_partition_1.part_type);
                    fprintf(report,"<tr><td>part_fit_1<td/><td>%c</td></tr>\n",mbrRead.mbr_partition_1.part_fit);
                    fprintf(report,"<tr><td>part_start_1<td/><td>%d</td></tr>\n",mbrRead.mbr_partition_1.part_start);
                    fprintf(report,"<tr><td>part_size_1<td/><td>%d</td></tr>\n",mbrRead.mbr_partition_1.part_size);
                    fprintf(report,"<tr><td>part_name_1<td/><td>%s</td></tr>\n",mbrRead.mbr_partition_1.part_name);
                }
                if(mbrRead.mbr_partition_2.part_status=='1'){
                    fprintf(report,"<tr><td>part_status_2<td/><td>%c</td></tr>\n",mbrRead.mbr_partition_2.part_status);
                    fprintf(report,"<tr><td>part_type_2<td/><td>%c</td></tr>\n",mbrRead.mbr_partition_2.part_type);
                    fprintf(report,"<tr><td>part_fit_2<td/><td>%c</td></tr>\n",mbrRead.mbr_partition_2.part_fit);
                    fprintf(report,"<tr><td>part_start_2<td/><td>%d</td></tr>\n",mbrRead.mbr_partition_2.part_start);
                    fprintf(report,"<tr><td>part_size_2<td/><td>%d</td></tr>\n",mbrRead.mbr_partition_2.part_size);
                    fprintf(report,"<tr><td>part_name_2<td/><td>%s</td></tr>\n",mbrRead.mbr_partition_2.part_name);
                }
                if(mbrRead.mbr_partition_3.part_status=='1'){
                    fprintf(report,"<tr><td>part_status_3<td/><td>%c</td></tr>\n",mbrRead.mbr_partition_3.part_status);
                    fprintf(report,"<tr><td>part_type_3<td/><td>%c</td></tr>\n",mbrRead.mbr_partition_3.part_type);
                    fprintf(report,"<tr><td>part_fit_3<td/><td>%c</td></tr>\n",mbrRead.mbr_partition_3.part_fit);
                    fprintf(report,"<tr><td>part_start_3<td/><td>%d</td></tr>\n",mbrRead.mbr_partition_3.part_start);
                    fprintf(report,"<tr><td>part_size_3<td/><td>%d</td></tr>\n",mbrRead.mbr_partition_3.part_size);
                    fprintf(report,"<tr><td>part_name_3<td/><td>%s</td></tr>\n",mbrRead.mbr_partition_3.part_name);
                }
                if(mbrRead.mbr_partition_4.part_status=='1'){
                    fprintf(report,"<tr><td>part_status_4<td/><td>%c</td></tr>\n",mbrRead.mbr_partition_4.part_status);
                    fprintf(report,"<tr><td>part_type_4<td/><td>%c</td></tr>\n",mbrRead.mbr_partition_4.part_type);
                    fprintf(report,"<tr><td>part_fit_4<td/><td>%c</td></tr>\n",mbrRead.mbr_partition_4.part_fit);
                    fprintf(report,"<tr><td>part_start_4<td/><td>%d</td></tr>\n",mbrRead.mbr_partition_4.part_start);
                    fprintf(report,"<tr><td>part_size_4<td/><td>%d</td></tr>\n",mbrRead.mbr_partition_4.part_size);
                    fprintf(report,"<tr><td>part_name_4<td/><td>%s</td></tr>\n",mbrRead.mbr_partition_4.part_name);
                }
                fprintf(report,"</table>\n");
                if(mbrRead.mbr_partition_1.part_type=='e'||mbrRead.mbr_partition_1.part_type=='E'){
                    ebr ebrR;
                    fseek(piv,mbrRead.mbr_partition_1.part_start,SEEK_SET);
                    fread(&ebrR,sizeof(ebr),1,piv);
                    int i =1;
                    while(ebrR.part_status=='1'){
                    fprintf(report,"<br/>EBR_%d<br/>\n",i);
                    fprintf(report,"<table border=1>\n");
                    fprintf(report,"<tr><td>part_status_1<td/><td>%c</td></tr>\n",ebrR.part_status);
                    fprintf(report,"<tr><td>part_fit_1<td/><td>%c</td></tr>\n",ebrR.part_fit);
                    fprintf(report,"<tr><td>part_start_1<td/><td>%d</td></tr>\n",ebrR.part_start);
                    fprintf(report,"<tr><td>part_size_1<td/><td>%d</td></tr>\n",ebrR.part_size);
                    fprintf(report,"<tr><td>part_next_1<td/><td>%d</td></tr>\n",ebrR.part_next);
                    fprintf(report,"<tr><td>part_name_1<td/><td>%s</td></tr>\n",ebrR.part_name);
                    fprintf(report,"</table>\n");
                    i++;
                    if(ebrR.part_next>0){
                        fseek(piv,ebrR.part_next,SEEK_SET);
                        fread(&ebrR,sizeof(ebr),1,piv);
                    }else break;

                    }

                }
                else if(mbrRead.mbr_partition_2.part_type=='e'||mbrRead.mbr_partition_2.part_type=='E'){
                    ebr ebrR;
                    fseek(piv,mbrRead.mbr_partition_2.part_start,SEEK_SET);
                    fread(&ebrR,sizeof(ebr),1,piv);
                    int i =1;
                    while(ebrR.part_status=='1'){
                    fprintf(report,"<br/>EBR_%d<br/>\n",i);
                    fprintf(report,"<table border=1>\n");
                    fprintf(report,"<tr><td>part_status_1<td/><td>%c</td></tr>\n",ebrR.part_status);
                    fprintf(report,"<tr><td>part_fit_1<td/><td>%c</td></tr>\n",ebrR.part_fit);
                    fprintf(report,"<tr><td>part_start_1<td/><td>%d</td></tr>\n",ebrR.part_start);
                    fprintf(report,"<tr><td>part_size_1<td/><td>%d</td></tr>\n",ebrR.part_size);
                    fprintf(report,"<tr><td>part_next_1<td/><td>%d</td></tr>\n",ebrR.part_next);
                    fprintf(report,"<tr><td>part_name_1<td/><td>%s</td></tr>\n",ebrR.part_name);
                    fprintf(report,"</table>\n");
                    i++;
                    if(ebrR.part_next>0){
                        fseek(piv,ebrR.part_next,SEEK_SET);
                        fread(&ebrR,sizeof(ebr),1,piv);
                    }else break;

                    }

                }else if(mbrRead.mbr_partition_3.part_type=='e'||mbrRead.mbr_partition_3.part_type=='E'){
                    ebr ebrR;
                    fseek(piv,mbrRead.mbr_partition_3.part_start,SEEK_SET);
                    fread(&ebrR,sizeof(ebr),1,piv);
                    int i =1;
                    while(ebrR.part_status=='1'){
                    fprintf(report,"<br/>EBR_%d<br/>\n",i);
                    fprintf(report,"<table border=1>\n");
                    fprintf(report,"<tr><td>part_status_1<td/><td>%c</td></tr>\n",ebrR.part_status);
                    fprintf(report,"<tr><td>part_fit_1<td/><td>%c</td></tr>\n",ebrR.part_fit);
                    fprintf(report,"<tr><td>part_start_1<td/><td>%d</td></tr>\n",ebrR.part_start);
                    fprintf(report,"<tr><td>part_size_1<td/><td>%d</td></tr>\n",ebrR.part_size);
                    fprintf(report,"<tr><td>part_next_1<td/><td>%d</td></tr>\n",ebrR.part_next);
                    fprintf(report,"<tr><td>part_name_1<td/><td>%s</td></tr>\n",ebrR.part_name);
                    fprintf(report,"</table>\n");
                    i++;
                    if(ebrR.part_next>0){
                        fseek(piv,ebrR.part_next,SEEK_SET);
                        fread(&ebrR,sizeof(ebr),1,piv);
                    }else break;

                    }

                }else if(mbrRead.mbr_partition_4.part_type=='e'||mbrRead.mbr_partition_4.part_type=='E'){
                    ebr ebrR;
                    fseek(piv,mbrRead.mbr_partition_4.part_start,SEEK_SET);
                    fread(&ebrR,sizeof(ebr),1,piv);
                    int i =1;
                    while(ebrR.part_status=='1'){
                    fprintf(report,"<br/>EBR_%d<br/>\n",i);
                    fprintf(report,"<table border=1>\n");
                    fprintf(report,"<tr><td>part_status_1<td/><td>%c</td></tr>\n",ebrR.part_status);
                    fprintf(report,"<tr><td>part_fit_1<td/><td>%c</td></tr>\n",ebrR.part_fit);
                    fprintf(report,"<tr><td>part_start_1<td/><td>%d</td></tr>\n",ebrR.part_start);
                    fprintf(report,"<tr><td>part_size_1<td/><td>%d</td></tr>\n",ebrR.part_size);
                    fprintf(report,"<tr><td>part_next_1<td/><td>%d</td></tr>\n",ebrR.part_next);
                    fprintf(report,"<tr><td>part_name_1<td/><td>%s</td></tr>\n",ebrR.part_name);
                    fprintf(report,"</table>\n");
                    i++;
                    if(ebrR.part_next>0){
                        fseek(piv,ebrR.part_next,SEEK_SET);
                        fread(&ebrR,sizeof(ebr),1,piv);
                    }else break;

                    }

                }



                fclose(piv);

                }

            strcpy(result,"</body></HTML><br/><br/>");
            fwrite(result,24,1,report);

            fclose(report);
            printf("Reporte de MBR generado exitosamente. Puede consultarlo en: %s \n",n.path);
        }else{
            printf("Error al crear el archivo de reporte;\n");
            return;
        }
    }else{
        printf("Error al crear el reporte.\n");
        return;
    }
}


void reporteDisk(atributeStruct n){
    char* result = (char*)malloc(100);
    char* ar= (char*)malloc(20);
    strcpy(ar,"/home/mia/r.dot");


    nodoDisco* disk = getIdDiscoMontado(n.id[2]);
    if(disk!=NULL){
        if(disk->particiones==0){
            printf("Disco no montado.\n");
            return;
        }
        FILE* report = fopen(ar,"w+");
        if(report){

            fprintf(report,"digraph structs{\n");
            fprintf(report,"\tstruct3 [shape=record,label=\"MBR");

            FILE* piv = fopen(disk->path,"rb+");
            if(piv){
                mbr mbrRead;
                partition nPartition;
                fseek(piv,0,SEEK_SET);
                fread(&mbrRead,sizeof(mbr),1,piv);

                if(mbrRead.mbr_partition_1.part_status=='1'){
                    if(mbrRead.mbr_partition_1.part_type=='P'||mbrRead.mbr_partition_1.part_type=='p'){
                        fprintf(report,"|Primaria\\n");
                        fprintf(report,mbrRead.mbr_partition_1.part_name);

                    }else if(mbrRead.mbr_partition_1.part_type=='E'||mbrRead.mbr_partition_1.part_type=='e'){
                        fprintf(report,"|{Extendida\\n");
                        fprintf(report,mbrRead.mbr_partition_1.part_name);
                        fprintf(report,"|{");

                        ebr ebrR;
                        fseek(piv,mbrRead.mbr_partition_1.part_start,SEEK_SET);
                        fread(&ebrR,sizeof(ebr),1,piv);
                        if(ebrR.part_status=='1'){
                            fprintf(report,"Logica\\n");
                            fprintf(report,ebrR.part_name);

                            while(ebrR.part_next!=-1){
                                fseek(piv,ebrR.part_next,SEEK_SET);
                                fread(&ebrR,sizeof(ebr),1,piv);
                                fprintf(report,"|Logica\\n");
                                fprintf(report,ebrR.part_name);
                            }
                        }

                        fprintf(report,"}}");
                    }

                }else{
                    fprintf(report,"|Libre");
                }

                //!!!!! empieza la 2

                if(mbrRead.mbr_partition_2.part_status=='1'){
                    if(mbrRead.mbr_partition_2.part_type=='P'||mbrRead.mbr_partition_2.part_type=='p'){
                        fprintf(report,"|Primaria\\n");
                        fprintf(report,mbrRead.mbr_partition_2.part_name);
                    }else if(mbrRead.mbr_partition_2.part_type=='E'||mbrRead.mbr_partition_2.part_type=='e'){
                        fprintf(report,"|{Extendida\\n");
                        fprintf(report,mbrRead.mbr_partition_2.part_name);
                        fprintf(report,"|{");
                        ebr ebrR;
                        fseek(piv,mbrRead.mbr_partition_2.part_start,SEEK_SET);
                        fread(&ebrR,sizeof(ebr),1,piv);
                        if(ebrR.part_status=='1'){
                            fprintf(report,"Logica\\n");
                            fprintf(report,ebrR.part_name);
                            while(ebrR.part_next!=-1){
                                fseek(piv,ebrR.part_next,SEEK_SET);
                                fread(&ebrR,sizeof(ebr),1,piv);
                                fprintf(report,"|Logica\\n");
                                fprintf(report,ebrR.part_name);

                            }
                        }
                        fprintf(report,"}}");
                    }

                }else{
                    fprintf(report,"|Libre");
                }
                //!!!empieza la 3
                if(mbrRead.mbr_partition_3.part_status=='1'){
                    if(mbrRead.mbr_partition_3.part_type=='P'||mbrRead.mbr_partition_3.part_type=='p'){
                        fprintf(report,"|Primaria\\n");
                        fprintf(report,mbrRead.mbr_partition_3.part_name);
                    }else if(mbrRead.mbr_partition_3.part_type=='E'||mbrRead.mbr_partition_3.part_type=='e'){
                        fprintf(report,"|{Extendida\\n");
                        fprintf(report,mbrRead.mbr_partition_3.part_name);
                        fprintf(report,"|{");
                        ebr ebrR;
                        fseek(piv,mbrRead.mbr_partition_3.part_start,SEEK_SET);
                        fread(&ebrR,sizeof(ebr),1,piv);
                        if(ebrR.part_status=='1'){
                            fprintf(report,"Logica\\n");
                            fprintf(report,ebrR.part_name);

                            while(ebrR.part_next!=-1){
                                fseek(piv,ebrR.part_next,SEEK_SET);
                                fread(&ebrR,sizeof(ebr),1,piv);
                                fprintf(report,"|Logica\\n");
                                fprintf(report,ebrR.part_name);
                            }
                        }

                        fprintf(report,"}}");
                    }

                }else{
                   fprintf(report,"|Libre");
                }


                //!!!empieza la 4
                if(mbrRead.mbr_partition_4.part_status=='1'){
                    if(mbrRead.mbr_partition_4.part_type=='P'||mbrRead.mbr_partition_4.part_type=='p'){
                        fprintf(report,"|Primaria\\n");
                        fprintf(report,mbrRead.mbr_partition_4.part_name);
                    }else if(mbrRead.mbr_partition_4.part_type=='E'||mbrRead.mbr_partition_4.part_type=='e'){
                        fprintf(report,"|{Extendida\\n");
                        fprintf(report,mbrRead.mbr_partition_4.part_name);
                        fprintf(report,"|{");
                        ebr ebrR;
                        fseek(piv,mbrRead.mbr_partition_4.part_start,SEEK_SET);
                        fread(&ebrR,sizeof(ebr),1,piv);
                        if(ebrR.part_status=='1'){
                            fprintf(report,"Logica\\n");
                            fprintf(report,ebrR.part_name);

                            while(ebrR.part_next!=-1){
                                fseek(piv,ebrR.part_next,SEEK_SET);
                                fread(&ebrR,sizeof(ebr),1,piv);
                                fprintf(report,"|Logica\\n");
                                fprintf(report,ebrR.part_name);

                            }
                        }

                        fprintf(report,"}}");
                    }

                }else{
                    fprintf(report,"|Libre");
                }
            }else{
                printf("Error en disco %s",disk->path);
                return;
            }
            fprintf(report,"\"];}");
            fclose(report);
            char* sent = (char*)malloc(200);
            strcpy(sent,"sudo dot -Tjpg /home/mia/r.dot -o \"");
            strcat(sent,n.path);
            strcat(sent,"\"");
            int x = system(sent);
            if(x==-1){
                printf("Error al graficar el reporte. \n");
                return;
            }else{
                printf("Reporte creado exitosamente. Puede consultarlo en: %s\n",n.path);
            }

        }else{
            printf("Error al crear el archivo de reporte;");
            return;
        }
    }else{
        printf("Error al crear el reporte.\n");
        return;
    }
}

void rep(atributeStruct n){
    if(n.path==NULL){
            printf("Error en parametro -path.\n");
            return;
        }
        else crearPadres(n.path);
    if(n.id==NULL){
            printf("Error en parametro -id.\n");
            return;
        }

    nodoParticion* aa= existeParticionMontadaId(n.id);
    if (aa==NULL){
        printf("Particion %s no ha sido montada. \n", n.id);
        return;
    }
    if(strcasecmp(n.name,"disk")==0){
        reporteDisk(n);
    }
    else if(strcasecmp(n.name,"mbr")==0){
        reporteMBR(n);
    }
    /*else if(strcasecmp(n.name,"bm_avd")==0){
        reporteBitmap(n,1);
        return;
    }
    else if(strcasecmp(n.name,"bm_dd")==0){
        reporteBitmap(n,2);
        return;
    }
    else if(strcasecmp(n.name,"bm_inode")==0){
        reporteBitmap(n,3);
        return;
    }
    else if(strcasecmp(n.name,"bm_block")==0){
        reporteBitmap(n,4);
        return;
    }
    else if(strcasecmp(n.name,"tree")==0){
        printf("Aun por implementarse.\n");
        return;
    }
    else if(strcasecmp(n.name,"sb")==0){
        reporteSuperBloque(n);
        //printf("Aun por implementarse.\n");
        return;
    }
    else if(strcasecmp(n.name,"file")==0){
        printf("Aun por implementarse.\n");
        return;
    }
    else if(strcasecmp(n.name,"dd_file")==0){
        printf("Aun por implementarse.\n");
        return;
    }
    else if(strcasecmp(n.name,"avds")==0){
        printf("Aun por implementarse.\n");
        return;
    }
    else if(strcasecmp(n.name,"log")==0){
        printf("Aun por implementarse.\n");
        return;
    }*/
    else{
        printf("Error en atributo -name %s.\n",n.name);
        return;
    }
}
