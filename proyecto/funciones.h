#ifndef FUNCIONES_H_INCLUDED
#define FUNCIONES_H_INCLUDED
#include <string.h>
#include "estructuras.h"
void mkdisk(atributeStruct n);
void rmdisk(atributeStruct n);
void fdisk(atributeStruct n,int tipoFDISK);
void listarMount();
void mount(atributeStruct n);
void umount(atributeStruct n);
partition posicionNombreParticionPrimaria(mbr m,char* name);
partition posicionParticionExtendida(mbr m);
#endif // FUNCIONES_H_INCLUDED
